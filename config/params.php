<?php

return [
    'adminEmail' => 'admin@example.com',
    'pathToImage' => '/images',
    'pathToDocs' => '/docs',
];
