<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name' => 'Восток-Запад',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'la37ZU9EXLlITJEzvqC9CnQYg-mEz7yW',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'translate' => [
            'class' => 'app\components\Translate',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'site/index',
                '<module:(admin)>' => '<module>/admin/index',
//                '<module:(admin)>/<action:[a-zA-Z0-9_\-]+>' => '<module>/admin/<action>',
                '<module:(admin)>/<controller:\w+>' => '<module>/<controller>/index',
                '<module:(admin)>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                '<module:(admin)>/<controller:\w+>/<action:\w+>/<params:\w+>' => '<module>/<controller>/<action>',
                '<action:\w+>' => 'site/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>/<params:\w+>' => '<controller>/<action>',
            ],
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\AdminModule',
            'as access' => [ // if you need to set access
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['adminPermission'],
                    ],
                ]
            ],
        ],
//            'account' => [
//                'class' => 'app\modules\AccountModule',
//                'as access' => [ // if you need to set access
//                    'class' => 'yii\filters\AccessControl',
//                    'rules' => [
//                        [
//                            'allow' => true,
//                            'roles' => ['accountPermission'],
//                        ],
//                    ]
//                ],
//            ],
    ],
    'params' => $params,
];

return $config;
