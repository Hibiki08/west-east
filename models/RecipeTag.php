<?php

namespace app\models;

use Yii;
use yii\db\Command;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "recipes_tags".
 *
 * @property integer $id
 * @property integer $tag_id
 * @property integer $recipe_id
 *
 */

class RecipeTag extends ActiveRecord {

    public static function tableName() {
        return 'recipes_tags';
    }
    
    public static function findByBond($tag_id, $recipe_id) {
        return self::find()
            ->where(['tag_id' => $tag_id])
            ->andWhere(['recipe_id' => $recipe_id])
            ->one();
    }

}