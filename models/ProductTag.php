<?php

namespace app\models;

use Yii;
use yii\db\Command;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "products_tags".
 *
 * @property integer $id
 * @property integer $tag_id
 * @property integer $product_id
 *
 */

class ProductTag extends ActiveRecord {

    public static function tableName() {
        return 'products_tags';
    }

    public static function findByBond($tag_id, $product_id) {
        return self::find()
            ->where(['tag_id' => $tag_id])
            ->andWhere(['product_id' => $product_id])
            ->one();
    }

}