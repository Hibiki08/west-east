<?php

namespace app\models;

use Yii;
use yii\db\Command;
use yii\db\Expression;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "packing_types".
 *
 * @property string $title
 *
 */


class PackingType extends ActiveRecord {

    public static function tableName() {
        return 'packing_types';
    }

    public static function findAllTypes() {
        return self::find()
            ->asArray()
            ->all();
    }

    public static function findByName($name) {
        return self::find()
            ->where(['title' => $name])
            ->one();
    }

}
