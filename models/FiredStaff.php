<?php

namespace app\models;

use Yii;
use yii\db\Command;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "fired_staff".
 *
 * @property integer $id
 * @property string $position
 * @property string $name
 * @property string $subdivision
 * @property string $dis_reason
 * @property string $details
 * @property integer $active
 *
 */


class FiredStaff extends ActiveRecord {

    public static function tableName() {
        return 'fired_staff';
    }

    public function rules() {
        return [
            [['name', 'position', 'subdivision'], 'string'],
        ];
    }

    public function getAll() {
        return self::find();
    }

    public static function findById($id) {
        return self::find()
            ->where(['id' => $id])
            ->one();
    }

    public function search($params) {
        $this->load($params);

        $query = $this->getAll();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', self::tableName() . '.name', $this->name]);
        $query->andFilterWhere(['like', self::tableName() . '.position', $this->position]);
        $query->andFilterWhere(['like', self::tableName() . '.subdivision', $this->subdivision]);

        return $dataProvider;
    }

    public function attributeLabels(){
        return [
            'position' => 'Должность',
            'name' => 'Имя сотрудника',
            'subdivision' => 'Подразделение',
            'dis_reason' => 'Причина увольнения',
            'details' => 'Подробности',
            'active' => 'Активность',
        ];
    }

}
