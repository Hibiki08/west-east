<?php

namespace app\models;

use Yii;
use yii\db\Command;
use yii\db\Expression;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "countries".
 *
 * @property string $title
 *
 */


class Country extends ActiveRecord {

    public static function tableName() {
        return 'countries';
    }

    public static function findAllCountries() {
        return self::find()
            ->asArray()
            ->all();
    }

    public static function findByName($name) {
        return self::find()
            ->where(['title' => $name])
            ->one();
    }

}
