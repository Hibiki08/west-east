<?php

namespace app\models;

use Yii;
use yii\db\Command;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "slides".
 *
 * @property integer $id
 * @property string $img
 * @property string $link
 * @property string $text
 * @property integer $sort
 * @property integer $type_id
 * @property integer $active
 *
 */

class Slide extends ActiveRecord {

    public static function tableName() {
        return 'slides';
    }
    
    public function getSliderName() {
        return $this->hasOne(SliderType::class, ['id' => 'type_id'])
            ->alias('slide');
    }
    
    public static function findByTypeId($id) {
        return self::find()
            ->where(['type_id' => $id])
            ->orderBy(['sort' => SORT_ASC])
//            ->asArray()
            ->all();
    }

    public static function findById($id) {
        return self::find()
            ->where(['id' => $id])
            ->one();
    }
    
}