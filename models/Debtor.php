<?php

namespace app\models;

use Yii;
use yii\db\Command;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "debtors".
 *
 * @property integer $id
 * @property string $company_name
 * @property integer $ind_number
 * @property integer $reg_number
 * @property string $address
 * @property string $member
 * @property string $director
 * @property string $court_decision
 * @property integer $active
 *
 */


class Debtor extends ActiveRecord {

    const SCENARIO_CREATE = 'create';

    public static function tableName() {
        return 'debtors';
    }

    public function rules() {
        return [
            [['company_name', 'address', 'member', 'director'], 'string'],
            [['ind_number', 'reg_number'], 'integer'],
        ];
    }
    
    public static function getAll() {
        return self::find()
            ->all();
    }

    public function search($params) {
        $this->load($params);

        $query = self::find()
            ->asArray()
            ->orderBy('id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', self::tableName() . '.company_name', $this->company_name]);
        $query->andFilterWhere(['like', self::tableName() . '.address', $this->address]);
        $query->andFilterWhere(['like', self::tableName() . '.member', $this->member]);
        $query->andFilterWhere(['like', self::tableName() . '.director', $this->director]);
        $query->andFilterWhere([self::tableName() . '.ind_number' => $this->ind_number]);
        $query->andFilterWhere([self::tableName() . '.reg_number' => $this->reg_number]);

        return $dataProvider;
    }

    public function attributeLabels(){
        return [
            'company_name' => 'Компания',
            'ind_number' => 'ИНН',
            'reg_number' => 'ОГРН',
            'address' => 'Адрес',
            'member' => 'Участник',
            'director' => 'Управляющий',
            'court_decision' => 'Решение суда',
            'active' => 'Активность',
        ];
    }
    
}
