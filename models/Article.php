<?php

namespace app\models;

use Yii;
use yii\db\Command;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "articles".
 *
 * @property integer $id
 * @property string $title
 * @property string $meta_title
 * @property string $meta_description
 * @property string $keywords
 * @property string $text
 * @property string $preview
 * @property string $date
 * @property integer $active
 *
 */

class Article extends ActiveRecord {

    public static function tableName() {
        return 'articles';
    }

    public function rules() {
        return [
            [['title'], 'string'],
            [['date'], 'safe']
        ];
    }

    public static function findAllNews() {
        return self::find()
            ->asArray()
            ->all();
    }

    public function search($params) {
        $this->load($params);

        $query = self::find()
            ->asArray()
            ->orderBy('id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!empty($this->date)) {
            $query->andFilterWhere(['like', 'date', Yii::$app->formatter->asDate($this->date, 'yyyy-MM-dd')]);
        }

        $query->andFilterWhere(['like', self::tableName() . '.title', $this->title]);

        return $dataProvider;
    }

}