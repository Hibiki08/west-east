<?php

namespace app\models\forms;

use Yii;


class ContactTypeForm extends AbstractForm {

    public $address;
    public $city_id;
    public $type_id;
    public $phone;
    public $phone_2;
    public $description;
    public $email;
    public $geocode;
    public $active;


    public function rules() {
        return [
            [['address', 'city_id'], 'required'],
            [['address', 'phone', 'phone_2', 'description'], 'string'],
            ['email', 'email'],
            ['geocode', 'match', 'pattern' => '/[0-9]{2}\.[0-9]+,[0-9]{2}\.[0-9]+$/i'],
            ['active', 'safe']
        ];
    }
    
    public function attributeLabels() {
        return [
            'address' => 'Адрес',
            'description' => 'Описание',
            'city_id' => 'Город',
            'phone' => 'Телефон',
            'phone_2' => 'Доп. телефон',
            'geocode' => 'Местоположение',
            'active' => 'Активность',
        ];
    }

}