<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\components\Translate;


abstract class AbstractForm extends Model {

    protected $imageDir = '';
    protected $docDir = '';

    public function upload($image) {
        if (isset($image->name) && !is_null($image->name)) {
            $translate = Yii::$app->translate;
            $image->name = $translate->translate($image->name);
            if ($image->saveAs(Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . $this->imageDir . '/' . $image->name)) {
                return $image->name;
            }
        }
        return null;
    }

    public function uploadDoc($doc) {
        if (isset($doc->name) && !is_null($doc->name)) {
            $translate = Yii::$app->translate;
            $doc->name = $translate->translate($doc->name);
            if ($doc->saveAs(Yii::$app->basePath . '/web' . Yii::$app->params['pathToDocs'] .  $this->docDir . '/' . $doc->name)) {
                return $doc->name;
            }
        }
        return null;
    }
    
    public function getImageDir() {
        return $this->imageDir;
    }

    public function getDocDir() {
        return $this->docDir;
    }
    
}