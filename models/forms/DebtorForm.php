<?php

namespace app\models\forms;

use Yii;


class DebtorForm extends AbstractForm {

    public $company_name;
    public $ind_number;
    public $reg_number;
    public $address;
    public $member;
    public $director;
    public $court_decision;
    public $active;

    protected $docDir = '/court_decisions';


    public function rules() {
        return [
            [['company_name', 'ind_number', 'reg_number', 'address', 'member', 'director'], 'required'],
            [['court_decision'], 'file', 'extensions' => ['pdf'], 'skipOnEmpty' => true],
            [['company_name', 'ind_number', 'reg_number', 'address', 'member', 'director'], 'filter', 'filter' => 'trim'],
            ['company_name', 'string', 'max' => 150],
            ['address', 'string', 'max' => 1000],
            ['member', 'string', 'max' => 250],
            ['director', 'string', 'max' => 250],
            [['ind_number', 'reg_number'], 'integer'],
            [['active'], 'safe'],
        ];
    }

    public function uploadImage($path, $image) {
        if (isset($image->name) && !is_null($image->name)) {
            $translate = Yii::$app->translate;
            $image->name = $translate->translate($image->name);

            $imagePath = Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . $this->imageDir . '/' . $path;
            if (!file_exists($imagePath)) {
                mkdir($imagePath);
            }

            if ($image->saveAs($imagePath . $image->name)) {
                return $image->name;
            }
        }
        return null;
    }

    public function attributeLabels(){
        return [
            'company_name' => 'Компания',
            'ind_number' => 'ИНН',
            'reg_number' => 'ОГРН',
            'address' => 'Адрес',
            'member' => 'Участник',
            'director' => 'Управляющий',
            'court_decision' => 'Решение суда',
            'active' => 'Активность',
        ];
    }

}
