<?php

namespace app\models\forms;

use Yii;


class RecipesForm extends AbstractForm {

    public $title;
    public $text;
    public $meta_title;
    public $meta_description;
    public $keywords;
    public $seo_url;
    public $preview;
    public $tags;
    public $active;

    protected $imageDir = '/recipes';

    public function rules() {
        return [
            [['preview'], 'file', 'extensions' => 'jpg, jpeg, gif, png', 'skipOnEmpty' => true, 'maxSize' => 1048576],
            [['title', 'text'], 'required'],
            [['text'], 'string'],
            ['title', 'string', 'max' => 250],
            ['seo_url', 'string', 'max' => 100],
            ['meta_title', 'string', 'max' => 300],
            ['meta_description', 'string', 'max' => 500],
            ['keywords', 'string', 'max' => 500],
            [['active', 'alt', 'tags'], 'safe'],
        ];
    }

    public function attributeLabels(){
        return [
            'title' => 'Название',
            'meta_title' => 'Meta title',
            'meta_description' => 'Meta description',
            'keywords' => 'Keywords',
            'seo_url' => 'SEO URL',
            'preview' => 'Превью',
            'tags' => 'Теги',
            'active' => 'Активность',
        ];
    }

}
