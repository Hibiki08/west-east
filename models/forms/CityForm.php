<?php

namespace app\models\forms;

use Yii;


class CityForm extends AbstractForm {

    public $title;


    public function rules() {
        return [
            [['title'], 'required'],
            ['title', 'string']
        ];
    }

    public function attributeLabels() {
        return [
            'title' => 'Название',
        ];
    }

}