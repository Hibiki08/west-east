<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\components\Translate;
use yii\web\UploadedFile;


class SlideForm extends AbstractForm {

    public $text;
    public $link;
    public $img;
    public $sort;
    public $active;

    protected $imageDir = '/sliders';
    
    const SCENARIO_CREATE = 'create';
    

    public function rules() {
        return [
            [['img'], 'file', 'extensions' => ['jpg', 'jpeg', 'gif', 'png'], 'skipOnEmpty' => !($this->scenario == self::SCENARIO_CREATE)],
            [['sort'], 'integer'],
            [['link', 'text'], 'filter', 'filter' => 'trim'],
            [['active'], 'safe'],
        ];
    }

    public function uploadImage($path, $image) {
        if (isset($image->name) && !is_null($image->name)) {
            $translate = Yii::$app->translate;
            $image->name = $translate->translate($image->name);

            $imagePath = Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . $this->imageDir . '/' . $path;
            if (!file_exists($imagePath)) {
                mkdir($imagePath);
            }

            if ($image->saveAs($imagePath . $image->name)) {
                return $image->name;
            }
        }
        return null;
    }

    public function beforeValidate() {
        $this->img = UploadedFile::getInstance($this, 'img');
        return parent::beforeValidate();
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['text', 'img', 'sort', 'link', 'active'];
        return $scenarios;
    }

    public function attributeLabels(){
        return [
            'img' => 'Слайд',
            'text' => 'Текст',
            'link' => 'Ссылка',
            'sort' => 'Сортировка',
            'active' => 'Активность',
        ];
    }

}