<?php

namespace app\models\forms;

use Yii;


class ProductForm extends AbstractForm {

    public $title;
    public $vendor_code;
    public $category_id;
    public $meta_title;
    public $meta_description;
    public $keywords;
    public $description;
    public $seo_url;
    public $item_weight;
    public $box_amount;
    public $unit;
    public $packing_type_id;
    public $brand_id;
    public $country_id;
    public $amount;
    public $preview;
    public $images;
    public $active;
    public $file;
    public $alt;
    public $tags;

    protected $imageDir = '/products';
    protected $docDir = '/import';

    const SCENARIO_IMPORT = 'import';
    

    public function rules() {
        return [
            [['preview'], 'file', 'extensions' => 'jpg, jpeg, gif, png', 'skipOnEmpty' => true, 'maxSize' => 1048576],
            [['file'], 'file', 'extensions' => 'xml', 'skipOnEmpty' => false, 'maxSize' => 10048576, 'except' => self::SCENARIO_DEFAULT],
            [['images'], 'file', 'extensions' => 'jpg, jpeg, gif, png', 'maxFiles' => 10, 'skipOnEmpty' => true],
            [['title'], 'required'],
            [['title', 'seo_url', 'meta_title', 'description', 'meta_description', 'keywords', 'preview', 'unit'], 'string'],
            [['vendor_code', 'category_id', 'box_amount', 'packing_type_id', 'brand_id', 'country_id', 'amount'], 'integer'],
            [['item_weight'], 'double'],
            [['active', 'alt', 'tags'], 'safe'],
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_IMPORT] = ['file'];
        return $scenarios;
    }

//    public function uploadXml($doc) {
//        if (isset($doc->name) && !is_null($doc->name)) {
//            $translate = Yii::$app->translate;
//            $doc->name = $translate->translate($doc->name);
//            if ($doc->saveAs(Yii::$app->basePath . '/web' . Yii::$app->params['pathToDocs'] .  Yii::$app->params['importDir'] . '/' . $doc->name)) {
//                return $doc->name;
//            }
//        }
//        return null;
//    }

    public function uploadImages($path, $image) {
        if (isset($image->name) && !is_null($image->name)) {
            $translate = Yii::$app->translate;
            $image->name = $translate->translate($image->name);
            if ($image->saveAs(Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . '/' . $path . $image->name)) {
                return true;
            }
        }
        return false;
    }
    
    public function attributeLabels(){
        return [
            'title' => 'Название',
            'vendor_code' => 'Артикул',
            'category_id' => 'Категория',
            'description' => 'Описание',
            'meta_title' => 'Meta title',
            'meta_description' => 'Meta description',
            'keywords' => 'Keywords',
            'seo_url' => 'SEO URL',
            'item_weight' => 'Вес за единицу',
            'box_amount' => 'Количество в коробке',
            'unit' => 'Единица измерения',
            'packing_type_id' => 'Вид первичной упаковки',
            'brand_id' => 'Производитель',
            'country_id' => 'Страна-производитель',
            'amount' => 'Количество на складе',
            'preview' => 'Превью',
            'active' => 'Активность',
            'file' => 'Импорт продуктов',
            'tags' => 'Теги',
        ];
    }

}