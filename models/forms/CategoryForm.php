<?php

namespace app\models\forms;

use Yii;


class CategoryForm extends AbstractForm {

    public $title;
    public $parent_id;
    public $seo_url;
    public $meta_title;
    public $description;
    public $meta_description;
    public $keywords;
    public $preview;
    public $status;
    public $tags;
    public $file;

    protected $docDir = '/import';

    const SCENARIO_IMPORT = 'import';


    public function rules() {
        return [
            [['preview'], 'file', 'extensions' => 'jpg, jpeg, gif, png', 'skipOnEmpty' => true, 'maxSize' => 1048576],
            [['file'], 'file', 'extensions' => 'xml', 'skipOnEmpty' => false, 'maxSize' => 10048576, 'except' => self::SCENARIO_DEFAULT],
            [['title'], 'required'],
            [['title', 'seo_url', 'meta_title', 'description', 'meta_description', 'keywords', 'preview'], 'string'],
            [['parent_id'], 'integer'],
            ['parent_id', 'default', 'value' => null],
            [ 'tags', 'safe']
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_IMPORT] = ['file'];
        return $scenarios;
    }
    
    public function attributeLabels() {
        return [
            'title' => 'Название',
            'description' => 'Описание',
            'meta_title' => 'Meta title',
            'meta_description' => 'Meta description',
            'preview' => 'Превью',
            'keywords' => 'Keywords',
            'parent_id' => 'Раздел',
            'status' => 'Активность',
            'file' => 'Импорт категорий',
            'tags' => 'Теги',
        ];
    }

}