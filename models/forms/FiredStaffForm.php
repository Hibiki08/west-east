<?php

namespace app\models\forms;

use Yii;


class FiredStaffForm extends AbstractForm {

    public $position;
    public $name;
    public $subdivision;
    public $dis_reason;
    public $details;
    public $active;


    public function rules() {
        return [
            [['name', 'position', 'subdivision', 'dis_reason', 'details'], 'required'],
            [['name'], 'string', 'max' => 250],
            [['position'], 'string', 'max' => 150],
            [['subdivision'], 'string', 'max' => 500],
            [['dis_reason'], 'string', 'max' => 500],
            [['details'], 'string', 'max' => 1000],
            [['active'], 'safe'],
        ];
    }

    public function attributeLabels(){
        return [
            'position' => 'Должность',
            'name' => 'Имя сотрудника',
            'subdivision' => 'Подразделение',
            'dis_reason' => 'Причина увольнения',
            'details' => 'Подробности',
            'active' => 'Активность',
        ];
    }

}
