<?php

namespace app\models\forms;

use Yii;


class NewsForm extends AbstractForm {

    public $title;
    public $meta_title;
    public $meta_description;
    public $keywords;
    public $text;
    public $preview;
    public $active;
    
    protected $imageDir = '/news';
    

    public function rules() {
        return [
            [['preview'], 'file', 'extensions' => 'jpg, jpeg, gif, png', 'skipOnEmpty' => true, 'maxSize' => 4048576],
            [['title'], 'required'],
            [['title', 'meta_title', 'text', 'meta_description', 'keywords', 'preview'], 'string'],
        ];
    }

    public function attributeLabels() {
        return [
            'title' => 'Название',
            'meta_title' => 'Meta title',
            'meta_description' => 'Meta description',
            'preview' => 'Превью',
            'keywords' => 'Keywords',
            'text' => 'Текст',
            'active' => 'Активность'
        ];
    }

}