<?php

namespace app\models;

use Yii;
use yii\db\Command;
use yii\db\Expression;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "brands".
 *
 * @property string $title
 *
 */



class Brand extends ActiveRecord {

    public static function tableName() {
        return 'brands';
    }

    public static function findAllBrands() {
        return self::find()
            ->asArray()
            ->all();
    }

    public static function findByName($name) {
        return self::find()
            ->where(['title' => $name])
            ->one();
    }

}
