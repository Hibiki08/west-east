<?php

namespace app\models;

use Yii;
use yii\db\Command;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "cities".
 *
 * @property integer $id
 * @property string $title
 *
 */


class City extends ActiveRecord {

    public static function tableName() {
        return 'cities';
    }

    public function rules() {
        return [
            [['title'], 'string']
        ];
    }

    public function getAll() {
        return self::find();
    }

    public static function findById($id) {
        return self::find()
            ->where(['id' => $id])
            ->one();
    }

    public function search($params) {
        $this->load($params);

        $query = $this->getAll();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', self::tableName() . '.title', $this->title]);

        return $dataProvider;
    }

}
