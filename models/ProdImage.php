<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "prod_images".
 *
 * @property integer $id
 * @property string $path
 * @property string $alt
 * @property integer $prod_id
 *
 */

class ProdImage extends ActiveRecord {

//    protected $imageDir = 'products/';
    

    public static function tableName() {
        return 'prod_images';
    }

    public static function findAllByProdId($id) {
        return self::find()
            ->where(['prod_id' => $id])
            ->all();
    }
    
}