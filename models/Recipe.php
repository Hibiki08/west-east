<?php

namespace app\models;

use Yii;
use yii\db\Command;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "recipes".
 *
 * @property integer $id
 * @property string $title
 * @property string $meta_title
 * @property string $meta_description
 * @property string $keywords
 * @property string $text
 * @property string $seo_url
 * @property string $preview
 * @property string $active
 *
 */

class Recipe extends ActiveRecord {

    const IMG_FOLDER = '/recipes';


    public static function tableName() {
        return 'recipes';
    }

    public function rules() {
        return [
            [['title', 'seo_url'], 'string'],
        ];
    }
    
    public function getTags() {
        return $this->hasMany(Tag::class, ['id' => 'tag_id'])
            ->viaTable(RecipeTag::tableName(), ['recipe_id' => 'id']);
    }
    
    public static function findAllRecipes() {
        return self::find()
            ->asArray()
            ->all();
    }

    public function search($params) {
        $this->load($params);

        $query = self::find()
            ->asArray()
            ->orderBy('id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', self::tableName() . '.title', $this->title]);
        $query->andFilterWhere(['like', self::tableName() . '.seo_url', $this->seo_url]);

        return $dataProvider;
    }

}