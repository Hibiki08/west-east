<?php

namespace app\models;

use Yii;
use yii\db\Command;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "categories_tags".
 *
 * @property integer $id
 * @property integer $tag_id
 * @property integer $cat_id
 *
 */

class CategoryTag extends ActiveRecord {

    public static function tableName() {
        return 'categories_tags';
    }

    public static function findByBond($tag_id, $cat_id) {
        return self::find()
            ->where(['tag_id' => $tag_id])
            ->andWhere(['cat_id' => $cat_id])
            ->one();
    }

}