<?php

namespace app\models;

use Yii;
use yii\db\Command;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "slider_types".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 *
 */

class SliderType extends ActiveRecord {

    const SLIDER_TOP = 1;

    public static function tableName() {
        return 'slider_types';
    }
    
    public static function findById($id) {
        return self::find()
            ->where(['id' => $id])
            ->one();
    }

}