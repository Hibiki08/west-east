<?php

namespace app\models;

use Yii;
use yii\db\Command;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "tags".
 *
 * @property integer $id
 * @property string $title
 *
 */

class Tag extends ActiveRecord {

    public static function tableName() {
        return 'tags';
    }
    
    public static function findByTitle($title) {
        return self::find()
            ->where(['title' => $title])
            ->one();
    }
    
}