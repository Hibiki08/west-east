<?php

namespace app\models;

use Yii;
use yii\db\Command;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property string $title
 * @property string $meta_title
 * @property string $meta_description
 * @property string $keywords
 * @property string $description
 * @property string $seo_url
 * @property integer $item_weight
 * @property integer $box_amount
 * @property string $unit
 * @property integer $packing_type_id
 * @property integer $brand_id
 * @property integer $country_id
 * @property integer $category_id
 * @property integer $active
 * @property integer $amount
 * @property string $preview
 * @property integer $vendor_code
 *
 */

class Product extends ActiveRecord {

    const IMG_FOLDER = 'products/';

    
    public static function tableName() {
        return 'products';
    }

    public function rules() {
        return [
            [['vendor_code', 'category_id'], 'integer'],
            [['title', 'seo_url'], 'string'],
        ];
    }
    
    public function getBrand() {
        return $this->hasOne(Brand::class, ['id' => 'brand_id']);
    }

    public function getCountry() {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }
    
    public function getPackingType() {
        return $this->hasOne(PackingType::class, ['id' => 'packing_type_id']);
    }

    public function getCategory() {
        return $this->hasOne(Category::class, ['id' => 'category_id'])
            ->alias('category');
    }

    public function getTags() {
        return $this->hasMany(Tag::class, ['id' => 'tag_id'])
            ->viaTable(ProductTag::tableName(), ['product_id' => 'id']);
    }

    public static function findAllProducts() {
        return self::find()
            ->asArray()
            ->joinWith('brand', true)
            ->joinWith('country', true)
            ->joinWith('packing_type', true)
            ->all();
    }

    public function search($params) {
        $this->load($params);
        
        $query = self::find()
            ->asArray()
            ->joinWith('category', true)
            ->orderBy('id');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $categories = [];
        $prent_cat = Category::findCategoryWithChildrenById($this->category_id);
        if (!empty($prent_cat)) {
            $categories[] = (int)$prent_cat['id'];

            if (!empty($prent_cat['children'])) {
                foreach ($prent_cat['children'] as $child) {
                    $categories[] = (int)$child['id'];

                    $child_cats = Category::findCategoryWithChildrenById($child['id']);

                    if (!empty($child_cats['children'])) {
                        foreach ($child_cats['children'] as $item) {
                            $categories[] = $item['id'];

                            $item_cats = Category::findCategoryWithChildrenById($item['id']);

                            if (!empty($item_cats['children'])) {
                                foreach ($item['children'] as $val) {
                                    $categories[] = (int)$val['id'];
                                }
                            }
                        }
                    }
                }
            }
        }

        $query->andFilterWhere(['like', self::tableName() . '.title', $this->title]);
        $query->andFilterWhere([self::tableName() . '.vendor_code' => $this->vendor_code]);
        $query->andFilterWhere(['like', self::tableName() . '.seo_url', $this->seo_url]);
        $query->andFilterWhere(['in', self::tableName() . '.category_id', $categories]);
        
        return $dataProvider;
    }

    public static function findByVendorCode($code) {
        return self::find()
            ->where(['vendor_code' => $code])
            ->one();
    }

}
