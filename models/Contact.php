<?php

namespace app\models;

use Yii;
use yii\db\Command;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "contacts".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $type_id
 * @property string $address
 * @property string $phone
 * @property string $phone_2
 * @property string $description
 * @property string $email
 * @property string $geocode
 * @property integer $active
 *
 */


class Contact extends ActiveRecord {

    public static function tableName() {
        return 'contacts';
    }
    
    public static function findBranches() {
        return self::find()
            ->where(['type_id' => ContactType::BRANCH_ID])
            ->all();
    }

    public static function findWarehouses() {
        return self::find()
            ->where(['type_id' => ContactType::WAREHOUSE_ID])
            ->all();
    }

    public static function findDistributors() {
        return self::find()
            ->where(['type_id' => ContactType::DISTRIBUTOR_ID])
            ->all();
    }
    
    public static function findById($id) {
        return self::find()
            ->where(['id' => $id])
            ->one();
    }

    public function getCity() {
        return $this->hasOne(City::class, ['id' => 'city_id'])
            ->alias('city');
    }

    public function search($params) {
        $this->load($params);

        $query = self::find()
            ->asArray()
            ->orderBy('id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

//        $query->andFilterWhere(['like', self::tableName() . '.title', $this->title]);
//        $query->andFilterWhere(['like', self::tableName() . '.seo_url', $this->seo_url]);

        return $dataProvider;
    }
    
}
