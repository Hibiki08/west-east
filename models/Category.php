<?php

namespace app\models;

use Yii;
use yii\db\Command;
use yii\db\Expression;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "categories".
 *
 * @property integer $id
 * @property string $title
 * @property string $meta_title
 * @property string $meta_description
 * @property string $keywords
 * @property string $description
 * @property integer $parent_id
 * @property string $cat_key
 * @property integer $level
 * @property integer $code
 * @property string $seo_url
 * @property string $preview
 * @property integer $status
 *
 */

class Category extends ActiveRecord {

    public static function tableName() {
        return 'categories';
    }

    public function rules() {
        return [
            [['parent_id'], 'integer']
        ];
    }

    public function getParent() {
        return $this->hasMany(self::class, ['id' => 'parent_id'])
            ->alias('parent');
    }

    public function getChildren() {
        return $this->hasMany(self::class, ['parent_id' => 'id'])
            ->alias('children');
    }

    public function getTags() {
        return $this->hasMany(Tag::class, ['id' => 'tag_id'])
            ->viaTable(CategoryTag::tableName(), ['cat_id' => 'id']);
    }


    public static function findParentWithChildren() {
        return self::find()
            ->joinWith('parent', true)
            ->asArray()
            ->alias('children')
            ->all();
    }
    
    public static function findAllCategories() {
        return self::find()
            ->asArray()
            ->all();
    } 
    
    public static function findById($id) {
        return self::findOne($id);
    }

    public static function findByCode($code) {
        return self::find()
            ->where(['code' => $code])
            ->one();
    }

    public static function findByKey($key) {
        return self::find()
            ->where(['cat_key' => $key])
            ->one();
    }

    public static function findCategoryWithChildrenById($id) {
        return self::find()
            ->joinWith('children', true)
            ->asArray()
            ->where(['parent.id' => $id])
            ->alias('parent')
            ->one();
    }

}