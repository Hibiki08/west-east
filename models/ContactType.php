<?php

namespace app\models;

use Yii;
use yii\db\Command;
use yii\db\Expression;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "contact_types".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 *
 */


class ContactType extends ActiveRecord {

    const BRANCH_ID = 1;
    const WAREHOUSE_ID = 2;
    const DISTRIBUTOR_ID = 3;

    public static function tableName() {
        return 'contact_types';
    }

    public static function findById($id) {
        return self::find()
            ->where(['id' => $id])
            ->one();
    }

}
