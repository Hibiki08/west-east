<?php

namespace app\modules\admin\controllers;

use Yii;
use PHPUnit\Runner\Exception;
use yii\web\Controller;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use yii\httpclient\Response;
use yii\web\NotFoundHttpException;
use app\models\Debtor;
use app\models\forms\DebtorForm;
use yii\web\UploadedFile;


class DebtorsController extends Controller {

    public $title = 'Должники';


    public function actionIndex() {
        $this->view->title = $this->title;
        $this->view->params['breadcrumbs'][] = $this->title;
        
        $searchModel = new Debtor();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $docDir = (new DebtorForm())->getDocDir();
        
        return $this->render('index', [
            'debtors' => $dataProvider,
            'searchModel' => $searchModel,
            'docDir' => $docDir
        ]);
    }

    public function actionUpdate($id = 0) {
        $this->view->title = !empty($id) ? 'Редактирование компании' : 'Добавление компании';
        $this->view->params['breadcrumbs'][] = ['label' => $this->title, 'url'=> Url::to(['/admin/debtors'])];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $form = new DebtorForm();
        $model = !empty($debtor = Debtor::findOne($id)) ? $debtor : new Debtor();

        if (empty($model)) {
            throw new NotFoundHttpException('Компания не найдена');
        }

        $docDir = $form->getDocDir();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $form->court_decision = UploadedFile::getInstance($form, 'court_decision');
            
            $model->company_name = $form->company_name;
            $model->ind_number = $form->ind_number;
            $model->reg_number = $form->reg_number;
            $model->address = $form->address;
            $model->member = $form->member;
            $model->director = $form->director;
            $model->active = isset(Yii::$app->request->post('DebtorForm')['active']) ? 1 : 0;

            if ($docName = $form->uploadDoc($form->court_decision)) {
                $model->court_decision = $docName;
            }

            if ($model->save() !== false) {
                $lastInsertID = $model->id;

                return Yii::$app->getResponse()->redirect(Url::to(['/admin/debtors/update', 'id' => $lastInsertID]));
            }
        }

        return $this->render('update', [
            'edit' => $form,
            'model' => $model,
            'docDir' => $docDir,
        ]);
    }

    public function actionDelete($id) {
        $model = Debtor::findOne($id);

        if (empty($model)) {
            throw new NotFoundHttpException('Компания не найдена');
        } else {
            if ($model->delete()) {
                return Yii::$app->getResponse()->redirect(Url::toRoute(['/admin/debtors']));
            }
        }
    }
    
}