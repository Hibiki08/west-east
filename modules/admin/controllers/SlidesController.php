<?php

namespace app\modules\admin\controllers;

use app\models\SliderType;
use Yii;
use yii\web\Controller;
use app\models\Slide;
use app\models\forms\SlideForm;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\data\ArrayDataProvider;
use app\components\ImageResizer;
use yii\web\NotFoundHttpException;
use yii\web\Response;


class SlidesController extends Controller {

    public $title = 'Слайдеры';


    public function actionIndex() {
        $this->view->title = $this->title;
        $this->view->params['breadcrumbs'][] = $this->title;

        $slider = SliderType::findById(SliderType::SLIDER_TOP);
        if (!empty($slider)) {
            $this->view->params['breadcrumbs'][] = $slider['description'];
        }
        $imageDir = (new SlideForm())->getImageDir();

        $slides = Slide::findByTypeId(SliderType::SLIDER_TOP);

        $arrayProvider = new ArrayDataProvider([
            'allModels' => $slides,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('slides', [
            'slides' => $arrayProvider,
            'imageDir' => $imageDir,
            'slider' => $slider,
        ]);
    }

    public function actionUpdate($id = 0, $type) {
        $this->view->title = !empty($id) ? 'Редактирование слайда' : 'Добавление слайда';
        $this->view->params['breadcrumbs'][] = ['label' => $this->title, 'url'=> Url::to(['/admin/slides'])];

        $slider_type = SliderType::findOne($type);

        if (empty($slider_type)) {
            throw new NotFoundHttpException('Слайдер не найден');
        }

        $slider = SliderType::findById(SliderType::SLIDER_TOP);
        if (!empty($slider)) {
            $this->view->params['breadcrumbs'][] = $slider['description'];
        }

        $id = !empty(Yii::$app->request->get('id')) ? Yii::$app->request->get('id') : null;

        $form = $id ? new SlideForm(['scenario' => SlideForm::SCENARIO_DEFAULT]) : new SlideForm(['scenario' => SlideForm::SCENARIO_CREATE]);
        $imageDir = $form->getImageDir();
        $model = $id ? Slide::findById($id) : new Slide();

        if (empty($model)) {
            throw new NotFoundHttpException('Слайд не найден');
        }

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $path = Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . $imageDir . '/' . $type;

            if ($imageName = $form->uploadImage($type . '/', $form->img)) {
                $folder = $imageDir . '/' . $type . '/';
                $resize = new ImageResizer($form->img->name, $folder, $folder, 172, '', 'mini');
                $resize->resize();
                $resize = new ImageResizer($form->img->name, $folder, $folder, 370, '', 'prev');
                $resize->resize();

                $model->img = $imageName;
            }

            $model->text = $form->text;
            $model->type_id = $type;
            $model->link = $form->link;
            $model->sort = $form->sort;
            $model->active = isset(Yii::$app->request->post('SlideForm')['active']) ? 1 : 0;
            $model->save();
            $lastInsertID = $model->id;

            return Yii::$app->getResponse()->redirect(Url::current(['id' => $lastInsertID, 'type' => $type]));
        }

        return $this->render('update', [
            'edit' => $form,
            'model' => $model,
            'imageDir' => $imageDir,
        ]);
    }

    public function actionSort() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $response = false;
            
            $id = (int)Yii::$app->request->post()['id'];
            $value = (int)Yii::$app->request->post()['value'];

            $slide = Slide::findById($id);

            if (!empty($slide)) {
                $slide->sort = $value;
                if ($slide->update() !== false) {
                    $response = true;
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $response,
            ];
        }
        Yii::$app->end();
    }

    public function actionDelete($id) {
        $model = Slide::findById($id);

        if (empty($model)) {
            throw new NotFoundHttpException('Слайд не найден');
        } else {
            if ($model->delete()) {
                $imageDir = (new SlideForm())->getImageDir();
                $path = Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . $imageDir . '/' . $model['type_id'];
                if (file_exists($path . '/' . $model->img)) {
                    unlink($path . '/' . $model->img);
                }
                if (file_exists($path . '/mini_' . $model->img)) {
                    unlink($path . '/mini_' . $model->img);
                }
                if (file_exists($path . '/prev_' . $model->img)) {
                    unlink($path . '/prev_' . $model->img);
                }

                return Yii::$app->getResponse()->redirect(Url::toRoute(['/admin/slides']));
            }
        }
    }
    
}