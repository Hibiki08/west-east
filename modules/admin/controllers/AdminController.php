<?php

namespace app\modules\admin\controllers;

use app\models\Ticket;
use app\models\UserCustomer;
use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use app\models\Newsletter;
use app\models\Request;
use app\models\RequestType;
use yii\data\ArrayDataProvider;
use yii\web\Response;
use app\models\User;
use app\models\Order;
//use yii\db\Query;


class AdminController extends Controller {

    const PAGE_SIZE = 20;

    public function actionIndex() {
        return $this->render('index');
    }
    
}