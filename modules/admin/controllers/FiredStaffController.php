<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\FiredStaff;
use app\models\forms\FiredStaffForm;
use PHPUnit\Runner\Exception;
use yii\web\Controller;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use yii\httpclient\Response;
use yii\web\NotFoundHttpException;


class FiredStaffController extends Controller {

    public $title = 'Уволенные сотрудники';


    public function actionIndex() {
        $this->view->title = $this->title;
        $this->view->params['breadcrumbs'][] = $this->title;

        $searchModel = new FiredStaff();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'staff' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionUpdate($id = 0) {
        $this->view->title = !empty($id) ? 'Редактирование сотрудники' : 'Добавление сотрудника';
        $this->view->params['breadcrumbs'][] = ['label' => $this->title, 'url'=> Url::to(['/admin/fired-staff'])];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $form = new FiredStaffForm();
        $model = !empty($staff = FiredStaff::findById($id)) ? $staff : new FiredStaff();

        if (empty($model)) {
            throw new NotFoundHttpException('Сотрудник не найден');
        }

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $model->name = $form->name;
            $model->position = $form->position;
            $model->subdivision = $form->subdivision;
            $model->dis_reason = $form->dis_reason;
            $model->details = $form->details;
            $model->active = isset(Yii::$app->request->post('FiredStaffForm')['active']) ? 1 : 0;

            if ($model->save() !== false) {
                $lastInsertID = $model->id;

                return Yii::$app->getResponse()->redirect(Url::to(['/admin/fired-staff/update', 'id' => $lastInsertID]));
            }
        }

        return $this->render('update', [
            'edit' => $form,
            'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        $model = FiredStaff::findById($id);

        if (empty($model)) {
            throw new NotFoundHttpException('Сотрудник не найден');
        } else {
            if ($model->delete()) {
                return Yii::$app->getResponse()->redirect(Url::toRoute(['/admin/fired-staff']));
            }
        }
    }
    
}