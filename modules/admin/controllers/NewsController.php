<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use app\models\Article;
use app\models\forms\NewsForm;
use yii\web\UploadedFile;
use app\components\ImageResizer;


class NewsController extends Controller {

    public $title = 'Новости';


    public function actionIndex() {
        $this->view->title = $this->title;
        $this->view->params['breadcrumbs'][] = $this->title;

        $searchModel = new Article();
        $imageDir = (new NewsForm())->getImageDir();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'news' => $dataProvider,
            'searchModel' => $searchModel,
            'imageDir' => $imageDir
        ]);
    }
    
    public function actionUpdate($id = 0) {
        $this->view->title = !empty($id) ? 'Редактирование новости' : 'Добавление новости';
        $this->view->params['breadcrumbs'][] = ['label' => $this->title, 'url'=> Url::to(['/admin/news'])];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $form = new NewsForm();
        $imageDir =  $form->getImageDir();
        $model = !empty($article = Article::findOne($id)) ? $article : new Article();

        if (empty($model)) {
            throw new NotFoundHttpException('Новость не найдена');
        }

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $form->preview = UploadedFile::getInstance($form, 'preview');
            if ($imageName = $form->upload($form->preview)) {
                
                if (!empty($model->preview)) {
                    $path = Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . $imageDir;
                    file_exists($path . '/' . $model->preview) ? unlink($path . '/' . $model->preview) : false;
                    file_exists($path . '/mini_' .$model->preview) ? unlink($path . '/mini_' . $model->preview) : false;
                    file_exists($path . '/prev_' . $model->preview) ? unlink($path . '/prev_' . $model->preview) : false;
                }
                $resize = new ImageResizer($form->preview->name, $imageDir . '/', $imageDir . '/', 172, '', 'mini');
                $resize->resize();
                $resize = new ImageResizer($form->preview->name, $imageDir . '/', $imageDir . '/', 370, '', 'prev');
                $resize->resize();

                $model->preview = $imageName;
            }

            $model->title = $form->title;
            $model->meta_title = $form->meta_title;
            $model->meta_description = $form->meta_description;
            $model->keywords = $form->keywords;
            $model->text = $form->text;
            $model->active = isset(Yii::$app->request->post('NewsForm')['active']) ? 1 : 0;

            if ($model->save() !== false) {
                $lastInsertID = $model->id;
                return Yii::$app->getResponse()->redirect(Url::to(['/admin/news/update', 'id' => $lastInsertID]));
            }
        }

        return $this->render('add_update', [
            'edit' => $form,
            'model' => $model,
            'imageDir' => $imageDir
        ]);
    }

    public function actionDelete($id) {
        $model = Article::findOne($id);

        if (empty($model)) {
            throw new NotFoundHttpException('Новость не найдена');
        } else {
            if ($model->delete()) {
                return Yii::$app->getResponse()->redirect(Url::toRoute(['/admin/news']));
            }
        }
    }

}