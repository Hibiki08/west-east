<?php

namespace app\modules\admin\controllers;

use app\models\City;
use app\models\ContactType;
use Yii;
use app\models\Contact;
use app\models\forms\ContactTypeForm;
use app\models\forms\CityForm;
use PHPUnit\Runner\Exception;
use yii\web\Controller;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use yii\httpclient\Response;
use yii\web\NotFoundHttpException;


class ContactsController extends Controller {

    public $title = 'Контакты';


    public function actionIndex() {
        return Yii::$app->getResponse()->redirect('/admin/contacts/branches');
    }
    
    public function actionBranches() {
        $this->view->title = 'Филиалы';
        $this->view->params['breadcrumbs'][] = $this->title;
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $branches = Contact::findBranches();


        $arrayProvider = new ArrayDataProvider([
            'allModels' => $branches,
            'pagination' => false,
        ]);
        
        return $this->render('contacts', [
            'contacts' => $arrayProvider,
        ]);
    }

    public function actionWarehouses() {
        $this->view->title = 'Склады';
        $this->view->params['breadcrumbs'][] = $this->title;
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $warehouses = Contact::findWarehouses();
        $type = ContactType::WAREHOUSE_ID;

        $arrayProvider = new ArrayDataProvider([
            'allModels' => $warehouses,
            'pagination' => false,
        ]);

        return $this->render('contacts', [
            'contacts' => $arrayProvider,
            'type' => $type
        ]);
    }

    public function actionDistributors() {
        $this->view->title = 'Дистрибьютеры';
        $this->view->params['breadcrumbs'][] = $this->title;
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $distributors = Contact::findDistributors();
        $type = ContactType::DISTRIBUTOR_ID;

        $arrayProvider = new ArrayDataProvider([
            'allModels' => $distributors,
            'pagination' => false,
        ]);
        
        return $this->render('contacts', [
            'contacts' => $arrayProvider,
            'type' => $type
        ]);
    }

    public function actionCities() {
        $this->view->title = 'Города';
        $this->view->params['breadcrumbs'][] = $this->title;
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $searchModel = new City();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('cities', [
            'cities' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionUpdate($type, $id = 0) {
        $contact_type = ContactType::findOne($type);

        if (empty($contact_type)) {
            throw new NotFoundHttpException('Контакт не найден');
        }

        $this->view->title = 'Редактирование контакта';
        $this->view->params['breadcrumbs'][] = $this->title;
        $this->view->params['breadcrumbs'][] = ['label' => $contact_type['description'], 'url'=> Url::to(['/admin/contacts/' . $contact_type['title']])];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $form = new ContactTypeForm();

        $allCities = (new City())->getAll()->all();

        $cities = [];
        if (!empty($allCities)) {
            foreach ($allCities as $city) {
                $cities[$city['id']] = $city['title'];
            }
        }

        $model = !empty($contact = Contact::findOne($id)) ? $contact : new Contact();
        
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $model->city_id = $form->city_id;
            $model->type_id = $type;
            $model->address = $form->address;
            $model->phone = $form->phone;
            $model->phone_2 = $form->phone_2;
            $model->description = $form->description;
            $model->email = $form->email;
            $model->geocode = $form->geocode;
            $model->active = isset(Yii::$app->request->post('ContactTypeForm')['active']) ? 1 : 0;

            if ($model->save() !== false) {
                $lastInsertID = $model->id;
                return Yii::$app->getResponse()->redirect(Url::current(['id' => $lastInsertID, 'type' => $type]));
            }
        }

        return $this->render('add_update', [
            'edit' => $form,
            'model' => $model,
            'cities' => $cities,
            'id' => $id,
            'contact_type' => $contact_type
        ]);
    }

    public function actionDelete($id) {
        $model = Contact::findById($id);

        if (empty($model)) {
            throw new NotFoundHttpException('Контакт не найден');
        } else {
            if ($model->delete()) {
                return Yii::$app->getResponse()->redirect(Yii::$app->request->referrer);
            }
        }
    }

    public function actionUpdateCity($id = 0) {
        $this->view->title = 'Редактирование города';
        $this->view->params['breadcrumbs'][] = $this->title;
        $this->view->params['breadcrumbs'][] = ['label' => 'Города', 'url'=> Url::to(['/admin/contacts/cities'])];
        $this->view->params['breadcrumbs'][] = $this->view->title;
        
        $model = !empty($city = City::findOne($id)) ? $city : new City();

        if (empty($model) && !empty($contact_type)) {
            throw new NotFoundHttpException('Город не найден');
        }

        $form = new CityForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $model->title = $form->title;

            if ($model->save() !== false) {
                $lastInsertID = $model->id;
                return Yii::$app->getResponse()->redirect(Url::current(['id' => $lastInsertID]));
            }
        }

        return $this->render('update_city', [
            'edit' => $form,
            'model' => $model,
            'id' => $id,
        ]);
    }

    public function actionDeleteCity($id) {
        $model = City::findById($id);

        if (empty($model)) {
            throw new NotFoundHttpException('Город не найден');
        } else {
            if ($model->delete()) {
                return Yii::$app->getResponse()->redirect(Yii::$app->request->referrer);
            }
        }
    }
    
}