<?php

namespace app\modules\admin\controllers;

use app\models\Brand;
use app\models\Country;
use app\models\PackingType;
use Yii;
use app\models\Category;
use yii\web\Controller;
use app\models\Product;
use app\models\ProdImage;
use app\models\forms\ProductForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use app\components\ImageResizer;
use yii\httpclient\Response;
use yii\httpclient\XmlParser;
use yii\web\Response as Resp;
use app\models\ProductTag;
use app\models\Tag;


class ProductsController extends Controller {

    public $title = 'Продукты';

    const LOG_FILE = '/runtime/logs/products_import.log';
    

    public function actionIndex() {
        $this->view->title = $this->title;
        $this->view->params['breadcrumbs'][] = $this->title;

        $searchModel = new Product();
        $imageDir = (new ProductForm())->getImageDir();
        $allCategories = Category::findAllCategories();
        $categories = [];

        if (!empty($allCategories)) {
            foreach ($allCategories as $product) {
                $categories[$product['id']] = $product['title'];
            }
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'products' => $dataProvider,
            'searchModel' => $searchModel,
            'categories' => $categories,
            'imageDir' => $imageDir
        ]);
    }

    public function actionUpdate($id = 0) {
        $this->view->title = !empty($id) ? 'Редактирование продукта' : 'Добавление продукта';
        $this->view->params['breadcrumbs'][] = ['label' => $this->title, 'url'=> Url::to(['/admin/products'])];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $form = new ProductForm();
        $images = $id ? ProdImage::findAllByProdId($id) : new ProdImage();
        $imageDir = $form->getImageDir();
        $model = !empty($product = Product::findOne($id)) ? $product : new Product();
        $allCategories = Category::findAllCategories();
        $categories = [];
        $allPackingTypes = PackingType::findAllTypes();
        $packingTypes = [];
        $allBrands = Brand::findAllBrands();
        $brands = [];
        $allCountries = Country::findAllCountries();
        $countries = [];

        if (!empty($allCategories)) {
            foreach ($allCategories as $product) {
                $categories[$product['id']] = $product['title'];
            }
        }
        if (!empty($allPackingTypes)) {
            foreach ($allPackingTypes as $type) {
                $packingTypes[$type['id']] = $type['title'];
            }
        }
        if (!empty($allBrands)) {
            foreach ($allBrands as $brand) {
                $brands[$brand['id']] = $brand['title'];
            }
        }
        if (!empty($allCountries)) {
            foreach ($allCountries as $country) {
                $countries[$country['id']] = $country['title'];
            }
        }

        if (empty($model)) {
            throw new NotFoundHttpException('Продукт не найден');
        }

        $tags = [];
        if (!empty($model->tags)) {
            foreach ($model->tags as $tag) {
                $tags[] = $tag->title;
            }
        }

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $form->preview = UploadedFile::getInstance($form, 'preview');
            $form->images = UploadedFile::getInstances($form, 'images');
            
            if ($imageName = $form->upload($form->preview)) {
                if (!empty($model->preview)) {
                    $path = Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . $imageDir;
                    file_exists($path . '/' . $model->preview) ? unlink($path . '/' . $model->preview) : false;
                    file_exists($path . '/mini_' .$model->preview) ? unlink($path . '/mini_' . $model->preview) : false;
                    file_exists($path . '/prev_' . $model->preview) ? unlink($path . '/prev_' . $model->preview) : false;
                }
                $resize = new ImageResizer($form->preview->name, $imageDir . '/', $imageDir . '/', 172, '', 'mini');
                $resize->resize();
                $resize = new ImageResizer($form->preview->name, $imageDir . '/', $imageDir . '/', 370, '', 'prev');
                $resize->resize();

                $model->preview = $imageName;
            }
            
            $model->title = $form->title;
            $model->vendor_code = $form->vendor_code;
            $model->category_id = $form->category_id;
            $model->description = $form->description;
            $model->seo_url = $form->seo_url;
            $model->meta_title = $form->meta_title;
            $model->meta_description = $form->meta_description;
            $model->keywords = $form->keywords;
            $model->item_weight = $form->item_weight;
            $model->box_amount = $form->box_amount;
            $model->unit = $form->unit;
            $model->packing_type_id = $form->packing_type_id;
            $model->brand_id = $form->brand_id;
            $model->country_id = $form->country_id;
            if (!empty($form->amount)) {
                $model->amount = $form->amount;
            }
            $model->active = isset(Yii::$app->request->post('ProductForm')['active']) ? 1 : 0;

            if ($model->save() !== false) {
                $path = Product::IMG_FOLDER . $id . '/';
                if (!file_exists(Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . '/' . $path)) {
                    mkdir(Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . '/' . $path);
                }

                if (!empty($form->images)) {
                    foreach ($form->images as $image) {
                        if ($form->uploadImages($path, $image)) {
                            $resizeAdminPrev = new ImageResizer($image->name, $path, $path, 172, '', 'mini');
                            $resizeAdminPrev->resize();
                            $resizeSlider = new ImageResizer($image->name, $path, $path, 225, '', 'mini_image');
                            $resizeSlider->resize();
                            
                            $prod_image = new ProdImage();
                            $prod_image->path = $image->name;
                            $prod_image->prod_id = $model->id;
                            $prod_image->save();
                        }
                    }
                }

                if (!empty($form->alt)) {
                    foreach ($form->alt as $id => $text) {
                        $image = ProdImage::findOne($id);
                        if (!empty($image)) {
                            $image->alt = $text;
                            $image->update();
                        }
                    }
                }
                
                $lastInsertID = $model->id;
                ProductTag::deleteAll(['product_id' => $lastInsertID]);

                $words = explode(',', $form->tags);
                if (!empty($words)) {
                    foreach ($words as $word) {
                        if (!empty($word)) {
                            $tag = Tag::findByTitle($word);
                            if (empty($tag)) {
                                $tag = new Tag();
                                $tag->title = $word;
                                $tag->save();
                            }

                            $bond = new ProductTag();
                            $bond->tag_id = $tag->id;
                            $bond->product_id = $lastInsertID;
                            $bond->save();
                        }
                    }
                }
                return Yii::$app->getResponse()->redirect(Url::to(['/admin/products/update', 'id' => $lastInsertID]));
            }
        }

        return $this->render('add_update', [
            'edit' => $form,
            'model' => $model,
            'categories' => $categories,
            'packingTypes' => $packingTypes,
            'brands' => $brands,
            'countries' => $countries,
            'imageDir' => $imageDir,
            'images' => $images,
            'tags' => $tags
        ]);
    }

    public function actionDeleteImages() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $response = false;

            $prod_id = (int)Yii::$app->request->post()['prod_id'];
            $image_id = (int)Yii::$app->request->post()['image_id'];
            $preview_id = (int)Yii::$app->request->post()['preview_id'];

            if ($preview_id) {
                $product = Product::findOne($prod_id);
                if (!empty($product)) {
                    $prevName = $product->preview;
                    $product->preview = null;
                    if ($product->update() !== false) {
                        unlink(Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . '/' . Product::IMG_FOLDER . $prevName);
                        unlink(Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . '/' . Product::IMG_FOLDER . 'mini_' . $prevName);
                        unlink(Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . '/' . Product::IMG_FOLDER . 'prev_' . $prevName);
                        $response = true;
                    }
                }
            }

            if ($image_id) {
                $image = ProdImage::findOne($image_id);
                if (!empty($image)) {
                    $path = Product::IMG_FOLDER . '/' . $prod_id . '/';
                    $imageName = $image->path;
                    if ($image->delete() !== false) {
                        unlink(Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . '/' . $path . $imageName);
                        unlink(Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . '/' . $path . 'mini_' . $imageName);
                        unlink(Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . '/' . $path . 'mini_image_' . $imageName);
                        $response = true;
                    }
                }
            }

            Yii::$app->response->format = Resp::FORMAT_JSON;
            return [
                'status' => $response,
            ];
        }
        Yii::$app->end();
    }

    public function actionDelete($id) {
        $model = Product::findOne($id);

        if (empty($model)) {
            throw new NotFoundHttpException('Категория не найдена');
        } else {
            if ($model->delete()) {
                return Yii::$app->getResponse()->redirect(Url::toRoute(['/admin/products']));
            }
        }
    }

    public function actionImport() {
        $this->view->title = 'Импорт продуктов';
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $form = new ProductForm(['scenario' => ProductForm::SCENARIO_IMPORT]);

        if ($form->load(Yii::$app->request->post())) {
            ini_set('memory_limit', '-1');
            $form->file = UploadedFile::getInstance($form, 'file');
            if ($doc = $form->uploadDoc($form->file)) {
                $file = file_get_contents(Yii::$app->basePath . '/web' . Yii::$app->params['pathToDocs'] .  '/import/' . $doc);
                if ($file) {
                    $response = new Response();
                    $response->setContent($file);
                    $response->addHeaders(['content-type' => 'text/xml; charset=windows-1251']);

                    $xml = new XmlParser();
                    $xmlArray = $xml->parse($response);

                    try {
                        if (!empty($xmlArray)) {
                            if (isset($xmlArray['Productheet'][0]['Table']['Row'])) {
                                $rows = $xmlArray['Productheet'][0]['Table']['Row'];
                                foreach ($rows as $row) {
                                    if (!empty($row['Cell'])) {
                                        $cat_key = isset($row['Cell'][0]['Data']) ? $row['Cell'][0]['Data'] : null;
                                        if (!empty($cat_key)) {
                                            $vendor_code = (int)$row['Cell'][2]['Data'];

                                            if (!empty($cat_key) && ($vendor_code > 0)) {
                                                $description = $row['Cell'][3]['Data'];
                                                $prod_brand = !empty($row['Cell'][4]['Data']) ? mb_strtolower($row['Cell'][4]['Data']) : null;
                                                $country_low = !empty($row['Cell'][5]['Data']) ? mb_strtolower($row['Cell'][5]['Data'], 'UTF-8') : null;
                                                $prod_country = mb_strtoupper(mb_substr($country_low, 0, 1)) . mb_substr($country_low, 1);
                                                $box_amount = $row['Cell'][7]['Data'];
                                                $item_weight = $row['Cell'][8]['Data'];
                                                $packing_type = !empty($row['Cell'][9]['Data']) ? $row['Cell'][9]['Data'] : null;
                                                $unit = mb_strtolower($row['Cell'][10]['Data'], 'UTF-8');

                                                $title_text = mb_strtolower($row['Cell'][13]['Data'], 'UTF-8');
                                                $title = mb_strtoupper(mb_substr($title_text, 0, 1)) . mb_substr($title_text, 1);

                                                $product = Product::findByVendorCode($vendor_code);
                                                if (empty($product)) {
                                                    $product = new Product();
                                                }

                                                $cat = Category::findByKey($cat_key);
                                                if (!empty($cat)) {
                                                    $product->category_id = $cat->id;
                                                }

                                                $product->title = $title;
                                                $product->vendor_code = $vendor_code;
                                                $product->description = $description;
                                                $product->item_weight = $item_weight;
                                                $product->box_amount = $box_amount;
                                                $product->unit = $unit;

                                                $packingType = PackingType::findByName($packing_type);
                                                if (empty($packingType) && !empty($packing_type)) {
                                                    $packingType = new PackingType();
                                                    $packingType->title = $packing_type;
                                                    $packingType->save();
                                                }

                                                $product->packing_type_id = $packingType['id'];

                                                $brand = Brand::findByName($prod_brand);
                                                if (empty($brand) && !empty($prod_brand)) {
                                                    $brand = new Brand();
                                                    $brand->title = $prod_brand;
                                                    $brand->save();
                                                }

                                                $product->brand_id = $brand['id'];

                                                $country = Country::findByName($prod_country);
                                                if (empty($country) && !empty($prod_country)) {
                                                    $country = new Country();
                                                    $country->title = $prod_country;
                                                    $country->save();
                                                }

                                                $product->country_id = $country['id'];
                                                $product->save();
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        Yii::$app->session->setFlash('success', 'Импорт прошёл успешно!');
                    } catch (\Exception $e) {
                        file_put_contents(Yii::$app->basePath . self::LOG_FILE, $e->getMessage(), FILE_APPEND );
                        Yii::$app->session->setFlash('error', 'Во время импорта произошла ошибка.');
                    }
                }

                return Yii::$app->getResponse()->redirect(Url::toRoute(['/admin/products/import']));
            }
        }

        return $this->render('import', [
            'edit' => $form,
        ]);
    }

}