<?php

namespace app\modules\admin\controllers;

use app\models\CategoryTag;
use PHPUnit\Runner\Exception;
use Yii;
use yii\web\Controller;
use app\models\Category;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;
use app\models\forms\CategoryForm;
use yii\helpers\Url;
use yii\httpclient\XmlParser;
use yii\httpclient\Response;
use yii\web\UploadedFile;
use app\models\Tag;


class CategoriesController extends Controller {

    public $title = 'Категории';

    const LOG_FILE = '/runtime/logs/categories_import.log';
    

    public function actionIndex() {
        $this->view->title = $this->title;
        $this->view->params['breadcrumbs'][] = $this->title;

        $categories = Category::findParentWithChildren();

        $arrayProvider = new ArrayDataProvider([
            'allModels' => $categories,
            'pagination' => false,
        ]);

        return $this->render('index', [
            'categories' => $arrayProvider
        ]);
    }

    public function actionAdd() {
        $this->view->title = 'Добавление категории';
        $this->view->params['breadcrumbs'][] = ['label' => $this->title, 'url'=> Url::to(['/admin/categories'])];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $action =  Yii::$app->controller->action->id;
        $id = !empty(Yii::$app->request->get('id')) ? Yii::$app->request->get('id') : null;
        $form = new CategoryForm();
        $model = new Category();
        $parentCategories = Category::findAllCategories();
        $parents[0] = 'Нет';

        if (!empty($parentCategories)) {
            foreach ($parentCategories as $parentCategory) {
                $parents[$parentCategory['id']] = $parentCategory['title'];
            }
        }

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $model->title = $form->title;
            $model->parent_id = $form->parent_id ? $form->parent_id : null;
            $model->seo_url = $form->seo_url;
            $model->meta_title = $form->meta_title;
            $model->description = $form->description;
            $model->meta_description = $form->meta_description;
            $model->keywords = $form->keywords;
            $model->status = isset(Yii::$app->request->post('CategoryForm')['status']) ? 1 : 0;

            if ($model->save() !== false) {
                $lastInsertID = $model->id;
                return Yii::$app->getResponse()->redirect(Url::to(['/admin/categories/update', 'id' => $lastInsertID]));
            }
        }

        return $this->render('add_update', [
            'edit' => $form,
            'model' => $model,
            'parentCategories' => $parentCategories,
            'parents' => $parents,
            'id' => $id,
            'action' => $action
        ]);
    }

    public function actionUpdate($id) {
        $this->view->title = 'Редактирование категории';
        $this->view->params['breadcrumbs'][] = ['label' => $this->title, 'url'=> Url::to(['/admin/categories'])];
        $this->view->params['breadcrumbs'][] = $this->view->title;
        $action =  Yii::$app->controller->action->id;
        
        $form = new CategoryForm();

        $parentCategories = Category::findAllCategories();
        $parents[0] = 'Нет';

        if (!empty($parentCategories)) {
            foreach ($parentCategories as $parentCategory) {
                $parents[$parentCategory['id']] = $parentCategory['title'];
            }
        }

        $model = Category::findById($id);

        if (empty($model)) {
            throw new NotFoundHttpException('Категория не найдена');
        }

        $tags = [];
        if (!empty($model->tags)) {
            foreach ($model->tags as $tag) {
                $tags[] = $tag->title;
            }
        }

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $model->title = $form->title;
            $model->parent_id = $form->parent_id ? $form->parent_id : null;
            $model->seo_url = $form->seo_url;
            $model->meta_title = $form->meta_title;
            $model->description = $form->description;
            $model->meta_description = $form->meta_description;
            $model->keywords = $form->keywords;
            $model->status = isset(Yii::$app->request->post('CategoryForm')['status']) ? 1 : 0;

            if ($model->save() !== false) {
                $lastInsertID = $model->id;
                CategoryTag::deleteAll(['cat_id' => $lastInsertID]);

                $words = explode(',', $form->tags);
                if (!empty($words)) {
                    foreach ($words as $word) {
                        if (!empty($word)) {
                            $tag = Tag::findByTitle($word);
                            if (empty($tag)) {
                                $tag = new Tag();
                                $tag->title = $word;
                                $tag->save();
                            }

                            $bond = new CategoryTag();
                            $bond->tag_id = $tag->id;
                            $bond->cat_id = $lastInsertID;
                            $bond->save();
                        }
                    }
                }
                return Yii::$app->getResponse()->redirect(Url::current(['id' => $lastInsertID]));
            }
        }

        return $this->render('add_update', [
            'edit' => $form,
            'model' => $model,
            'parentCategories' => $parentCategories,
            'parents' => $parents,
            'id' => $id,
            'action' => $action,
            'tags' => $tags
        ]);
    }

    public function actionDelete($id) {
        $model = Category::findById($id);

        if (empty($model)) {
            throw new NotFoundHttpException('Категория не найдена');
        } else {
            if ($model->delete()) {
                return Yii::$app->getResponse()->redirect(Url::toRoute(['/admin/categories']));
            }
        }
    }

    public function actionImport() {
        $this->view->title = 'Импорт категорий';
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $form = new CategoryForm(['scenario' => CategoryForm::SCENARIO_IMPORT]);

        if ($form->load(Yii::$app->request->post())) {
            ini_set('memory_limit', '-1');
            $form->file = UploadedFile::getInstance($form, 'file');
            if ($doc = $form->uploadDoc($form->file)) {
                $file = file_get_contents(Yii::$app->basePath . '/web' . Yii::$app->params['pathToDocs'] .  '/import/' . $doc);
                if ($file) {
                    $response = new Response();
                    $response->setContent($file);
                    $response->addHeaders(['content-type' => 'text/xml; charset=windows-1251']);

                    $xml = new XmlParser();
                    $xmlArray = $xml->parse($response);

                    try {
                        if (!empty($xmlArray)) {
                            if (isset($xmlArray['Worksheet']['Table']['Row'])) {
                                $rows = $xmlArray['Worksheet']['Table']['Row'];
                                foreach ($rows as $row) {
                                    if (!empty($row['Cell'])) {
                                        $cat_key = $row['Cell'][0]['Data'];
                                        $level = $row['Cell'][3]['Data'];


                                        if (!empty($cat_key) && strlen($level) == 1) {
                                            $text = mb_strtolower($row['Cell'][1]['Data'], 'UTF-8');
                                            $title = mb_strtoupper(mb_substr($text, 0, 1)) . mb_substr($text, 1);
                                            $code = $row['Cell'][2]['Data'];

                                            $translate = Yii::$app->translate;
                                            $seo_url = str_replace(['«','»','"','/', '(', ')'], '', $translate->translate(mb_strtolower($title)));

                                            $category = Category::findByCode($code);
                                            if (empty($category)) {
                                                $category = new Category();
                                            }

                                            if ($level == 0) {
                                                $category->parent_id = null;
                                            } else {
                                                $arrKeys = explode('.', $cat_key);
                                                array_pop($arrKeys);
                                                $key = implode('.', $arrKeys);
                                                $parent_cat = Category::findByKey($key);
                                                if (!empty($parent_cat)) {
                                                    $category->parent_id = $parent_cat->id;
                                                } else {
                                                    $category->parent_id = null;
                                                }
                                            }

                                            $category->cat_key = $cat_key;
                                            $category->level = $level;
                                            $category->title = $title;
                                            $category->seo_url = $seo_url;
                                            $category->code = $code;
                                            $category->save();
                                        }
                                    }
                                }
                            }
                        }

                        Yii::$app->session->setFlash('success', 'Импорт прошёл успешно!');
                    } catch (\Exception $e) {
                        file_put_contents(Yii::$app->basePath . self::LOG_FILE, $e->getMessage(), FILE_APPEND );
                        Yii::$app->session->setFlash('error', 'Во время импорта произошла ошибка.');
                    }
                }

                return Yii::$app->getResponse()->redirect(Url::toRoute(['/admin/categories/import']));
            }
        }

        return $this->render('import', [
            'edit' => $form,
        ]);
    }

}