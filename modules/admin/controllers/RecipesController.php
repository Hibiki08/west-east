<?php

namespace app\modules\admin\controllers;

use app\models\RecipeTag;
use Yii;
use app\models\Recipe;
use app\models\Tag;
use yii\web\Controller;
use yii\web\Response;
use app\models\forms\RecipesForm;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use app\components\ImageResizer;


class RecipesController extends Controller {

    public $title = 'Рецепты';
    
    
    public function actionIndex() {
        $this->view->title = $this->title;
        $this->view->params['breadcrumbs'][] = $this->title;

//        $recipes = Recipe::findAllRecipes();
        $searchModel = new Recipe();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $imageDir = (new RecipesForm())->getImageDir();

        return $this->render('index', [
            'recipes' => $dataProvider,
            'imageDir' => $imageDir,
            'searchModel' => $searchModel
        ]);
    }

    public function actionUpdate($id = 0) {
        $this->view->title = !empty($id) ? 'Редактирование рецепта' : 'Добавление рецепта';
        $this->view->params['breadcrumbs'][] = ['label' => $this->title, 'url'=> Url::to(['/admin/recipes'])];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        $form = new RecipesForm();
        $imageDir = $form->getImageDir();
        $model = !empty($recipe = Recipe::findOne($id)) ? $recipe : new Recipe();

        $tags = [];
        if (!empty($model->tags)) {
            foreach ($model->tags as $tag) {
                $tags[] = $tag->title;
            }
        }

        if (empty($model)) {
            throw new NotFoundHttpException('Рецепт не найден');
        }

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $form->preview = UploadedFile::getInstance($form, 'preview');

            $model->title = $form->title;
            $model->text = $form->text;
            $model->seo_url = $form->seo_url;
            $model->meta_title = $form->meta_title;
            $model->meta_description = $form->meta_description;
            $model->keywords = $form->keywords;
            $model->active = isset(Yii::$app->request->post('RecipesForm')['active']) ? 1 : 0;

            if ($imageName = $form->upload($form->preview)) {
                if (!empty($model->preview)) {
                    $path = Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . $imageDir;
                    file_exists($path . '/' . $model->preview) ? unlink($path . '/' . $model->preview) : false;
                    file_exists($path . '/mini_' .$model->preview) ? unlink($path . '/mini_' . $model->preview) : false;
                    file_exists($path . '/prev_' . $model->preview) ? unlink($path . '/prev_' . $model->preview) : false;
                }
                $resize = new ImageResizer($form->preview->name, $imageDir . '/', $imageDir . '/', 172, '', 'mini');
                $resize->resize();
                $resize = new ImageResizer($form->preview->name, $imageDir . '/', $imageDir . '/', 370, '', 'prev');
                $resize->resize();

                $model->preview = $imageName;
            }

            if ($model->save() !== false) {
                $lastInsertID = $model->id;

                RecipeTag::deleteAll(['recipe_id' => $lastInsertID]);

                $words = explode(',', $form->tags);
                if (!empty($words)) {
                    foreach ($words as $word) {
                        if (!empty($word)) {
                            $tag = Tag::findByTitle($word);
                            if (empty($tag)) {
                                $tag = new Tag();
                                $tag->title = $word;
                                $tag->save();
                            }

                            $bond = new RecipeTag();
                            $bond->tag_id = $tag->id;
                            $bond->recipe_id = $lastInsertID;
                            $bond->save();
                        }
                    }
                }
                return Yii::$app->getResponse()->redirect(Url::to(['/admin/recipes/update', 'id' => $lastInsertID]));
            }
        }

        return $this->render('add_update', [
            'edit' => $form,
            'model' => $model,
            'imageDir' => $imageDir,
            'tags' => $tags
        ]);
    }

    public function actionDeletePreview() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $response = false;

            $recipe_id = (int)Yii::$app->request->post()['recipe_id'];
            $preview_id = (int)Yii::$app->request->post()['preview_id'];
            
            if ($preview_id) {
                $recipe = Recipe::findOne($recipe_id);
                if (!empty($recipe)) {
                    $prevName = $recipe->preview;
                    $recipe->preview = null;
                    if ($recipe->update() !== false) {
                        unlink(Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . '/' . Recipe::IMG_FOLDER . '/' . $prevName);
                        unlink(Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . '/' . Recipe::IMG_FOLDER . '/' . 'mini_' . $prevName);
                        unlink(Yii::$app->basePath . '/web' . Yii::$app->params['pathToImage'] . '/' . Recipe::IMG_FOLDER . '/' . 'prev_' . $prevName);
                        $response = true;
                    }
                }
            }
            
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $response,
            ];
        }
        Yii::$app->end();
    }

    public function actionDelete($id) {
        $model = Recipe::findOne($id);

        if (empty($model)) {
            throw new NotFoundHttpException('Категория не найдена');
        } else {
            if ($model->delete()) {
                return Yii::$app->getResponse()->redirect(Url::toRoute(['/admin/recipes']));
            }
        }
    }
    
}