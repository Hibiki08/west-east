<?php
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

?>
    <h1><?php echo $this->title; ?></h1>

<?php $form = ActiveForm::begin([
    'options' => ['enctype'=>'multipart/form-data', 'class' => 'form-horizontal',],
    'fieldConfig' => [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10">{input}</div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
<?php echo $form->field($edit, 'city_id')->dropDownList($cities, ['options' => [$model['city_id'] => ['selected' => true]]]); ?>
<?php echo $form->field($edit, 'address')->input('text', ['value' => $model['address']]); ?>
<?php echo $form->field($edit, 'description')->widget(CKEditor::className(), [
    'editorOptions' => ElFinder::ckeditorOptions(['elfinder'], [
        'preset' => 'full',
        'inline' => false,
    ]),
    'options' => [
        'value' => $model['description']
    ]
]); ?>
<?php echo $form->field($edit, 'phone')->input('text', ['value' => $model['phone']]); ?>
<?php echo $form->field($edit, 'phone_2')->input('text', ['value' => $model['phone_2']]); ?>
<?php echo $form->field($edit, 'email')->input('text', ['value' => $model['email']]); ?>
<?php echo $form->field($edit, 'geocode')->input('text', [
    'value' => $model['geocode'],
    'placeholder' => '57.4446512,50.0603927'
]); ?>
<?php echo $form->field($edit, 'active')->input('checkbox', [
    'checked' => $model['active'] == 1 ? 'checked' : false,
    'class' => 'checkbox',
]) ?>
    <div class="form-group">
        <div class="col-lg-10 col-lg-offset-2">
            <a href="<?php echo Url::toRoute(['/admin/contacts/' . $contact_type['title']]); ?>" class="btn btn-warning">Вернуться к списку</a>
            <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>