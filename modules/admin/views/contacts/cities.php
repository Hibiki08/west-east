<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use app\models\forms\ProductForm;

?>

    <h1><?php echo Html::encode($this->title); ?></h1>
    <p><?php echo Html::a('Добавить город', ['update-city'], ['class' => 'btn btn-success']); ?></p>

<?php echo GridView::widget([
    'dataProvider' => $cities,
    'filterModel' => $searchModel,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
    'summary' => '',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id:text:ID',
        [
            'attribute' => 'title',
            'label' => 'Название',
            'value' => function ($model, $key, $index, $column) {
                return $model['title'];
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['/admin/contacts/update-city', 'id' => $model['id']]));
                },
                'delete' => function ($url, $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to(['/admin/contacts/delete-city', 'id' => $model['id']]), [
                        'title' => 'Delete',
                        'aria-label' => 'Delete',
                        'data-confirm' => 'Are you sure you want to delete this item?'
                    ]);
                },
            ]
        ],
    ],
]); ?>