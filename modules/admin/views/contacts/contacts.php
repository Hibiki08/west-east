<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

?>

    <h1><?php echo Html::encode($this->title); ?></h1>
    <p><?php echo Html::a('Добавить контакт', ['update', 'type' => $type], ['class' => 'btn btn-success']); ?></p>

<?php echo GridView::widget([
    'dataProvider' => $contacts,
//    'filterModel' => $searchModel,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
    'summary' => '',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id:text:ID',
        [
            'attribute' => 'address',
            'label' => 'Адрес',
            'value' => function ($model, $key, $index, $column) {
                return $model['address'];
            },
            'format' => 'html',
        ],
        [
            'attribute' => 'phone',
            'label' => 'Телефоны',
            'value' => function ($model, $key, $index, $column) {
                return $model['phone'] . '<br>' . $model['phone_2'];
            },
            'format' => 'html',
        ],
        [
            'attribute' => 'email',
            'label' => 'Email',
            'value' => function ($model, $key, $index, $column) {
                return $model['email'];
            },
        ],
        [
            'attribute' => 'city_id',
            'label' => 'Город',
            'value' => function ($model, $key, $index, $column) {
                if (!empty($model->city)) {
                    return $model->city['title'];
                }
            },
        ],
        [
            'attribute' => 'active',
            'label' => 'Активность',
            'value' => function ($model, $key, $index, $column) {
                return $model['active'] ? 'Да' : 'Нет';
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['/admin/contacts/update', 'id' => $model['id'], 'type' => $model['type_id']]));
                },
                'delete' => function ($url, $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to(['/admin/contacts/delete', 'id' => $model['id']]), [
                        'title' => 'Delete',
                        'aria-label' => 'Delete',
                        'data-confirm' => 'Are you sure you want to delete this item?'
                    ]);
                },
            ]
        ],
    ],
]); ?>