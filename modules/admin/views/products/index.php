<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use app\models\forms\ProductForm;

?>

    <h1><?php echo Html::encode($this->title); ?></h1>
    <p><?php echo Html::a('Добавить продукт', ['update'], ['class' => 'btn btn-success']); ?></p>

    <?php echo GridView::widget([
        'dataProvider' => $products,
        'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        'summary' => '',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id:text:ID',
            [
                'attribute' => 'preview',
                'label' => 'Превью',
                'value' => function ($model, $key, $index, $column) use ($imageDir) {
                    return !empty($model['preview']) ? '<img src="/images' . $imageDir . '/mini_' . $model['preview'] : '';
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'title',
//                'attribute' => 'productTitle',
                'label' => 'Название',
                'value' => function ($model, $key, $index, $column) {
                    return $model['title'];
                },
            ],
            [
                'attribute' => 'vendor_code',
                'label' => 'Артикул',
                'value' => function ($model, $key, $index, $column) {
                    return $model['vendor_code'];
                },
            ],
            [
                'attribute' => 'seo_url',
                'label' => 'SEO URL',
                'value' => function ($model, $key, $index, $column) {
                    return $model['seo_url'];
                },
            ],
            [
                'attribute' => 'category_id',
                'label' => 'Категория',
                'value' => function ($model, $key, $index, $column) {
                    return $model['category']['title'];
                },
                'filter' => $categories,
            ],
            [
                'attribute' => 'amount',
                'label' => 'Количество на складе',
                'value' => function ($model, $key, $index, $column) {
                    return $model['amount'];
                },
            ],
            [
                'attribute' => 'active',
                'label' => 'Активность',
                'value' => function ($model, $key, $index, $column) {
                    return $model['active'] ? 'Да' : 'Нет';
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>