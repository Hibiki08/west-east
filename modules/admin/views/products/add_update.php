<?php
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Product;
use wbraganca\tagsinput\TagsinputWidget;

?>
    <h1><?php echo $this->title; ?></h1>

<?php $form = ActiveForm::begin([
    'options' => ['enctype'=>'multipart/form-data', 'class' => 'form-horizontal',],
    'fieldConfig' => [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10">{input}</div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
<?php echo $form->field($edit, 'title')->input('text', ['value' => $model->title]); ?>
<?php echo $form->field($edit, 'vendor_code')->input('text', ['value' => $model->vendor_code]); ?>
<?php echo $form->field($edit, 'category_id')->dropDownList($categories, ['options' => [$model['category_id'] => ['selected' => true]]]); ?>
<?php echo $form->field($edit, 'description')->widget(CKEditor::className(), [
    'editorOptions' => ElFinder::ckeditorOptions(['elfinder'], [
        'preset' => 'full',
        'inline' => false,
    ]),
    'options' => [
        'value' => $model['description']
    ]
]); ?>
<?php echo $form->field($edit, 'seo_url')->input('text', ['value' => $model->seo_url]); ?>
<?php echo $form->field($edit, 'meta_title')->input('text', ['value' => $model['meta_title']]); ?>
<?php echo $form->field($edit, 'meta_description')->input('text', ['value' => $model['meta_description']]); ?>
<?php echo $form->field($edit, 'keywords')->input('text', ['value' => $model['keywords']]); ?>
<?php echo $form->field($edit, 'item_weight')->input('text', ['value' => $model['item_weight']]); ?>
<?php echo $form->field($edit, 'box_amount')->input('text', ['value' => $model['box_amount']]); ?>
<?php echo $form->field($edit, 'unit')->input('text', ['value' => $model['unit']]); ?>
<?php echo $form->field($edit, 'packing_type_id')->dropDownList($packingTypes, ['options' => [$model['packing_type_id'] => ['selected' => true]]]); ?>
<?php echo $form->field($edit, 'brand_id')->dropDownList($brands, ['options' => [$model['brand_id'] => ['selected' => true]]]); ?>
<?php echo $form->field($edit, 'country_id')->dropDownList($countries, ['options' => [$model['country_id'] => ['selected' => true]]]); ?>
<?php echo $form->field($edit, 'amount')->input('text', ['value' => $model['amount']]); ?>
<?php echo $form->field($edit, 'preview', ['options' => [
    'id' => 'preview-file',
    'class' => 'form-group field-editnewsform-preview'
]])->fileInput()->label('Загрузить превью');
if (isset($model->preview)) { ?>
        <label class="col-lg-2 control-label"></label>
        <div class="slides preview">
            <figure>
                <img class="img-thumbnail" src="<?php echo Yii::$app->params['pathToImage'] . $imageDir . '/mini_' . $model->preview; ?>">
            </figure>
            <span class="glyphicon glyphicon-remove" data-preview-id="<?php echo $model->id; ?>"></span>
        </div>
<?php } ?>
<?php echo @$form->field($edit, 'images[]')->fileInput(['multiple' => true, 'accept' => 'image/*'])->label('Загрузить картинки'); ?>
<?php if (!empty($images)) { ?>
    <div class="other-slides">
        <?php foreach ($images as $image) {
            if (isset($image->path)) { ?>
                <div>
                    <label class="col-lg-2 control-label"></label>
                    <div class="slides">
                        <figure>
                            <img class="img-rounded" src="<?php echo Yii::$app->params['pathToImage'] . '/' . Product::IMG_FOLDER . $model->id . '/mini_' . $image->path; ?>">
                        </figure>
                        <?php echo $form->field($edit, 'alt[' . $image->id . ']', ['template'=>'{input}'])->input('text', ['value' => $image->alt, 'class' => 'form-control image']); ?>
                        <span class="glyphicon glyphicon-remove" data-image-id="<?php echo $image->id; ?>"></span>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
<?php } ?>
<?php echo $form->field($edit, 'tags')->widget(TagsinputWidget::class, [
    'clientOptions' => [
        'trimValue' => true,
        'allowDuplicates' => false,
    ]
])->textInput([
    'value' => implode(',', $tags),
    'data-id' => $model->id
]); ?>
<?php echo $form->field($edit, 'active')->input('checkbox', [
    'checked' => $model['active'] == 1 ? 'checked' : false,
    'class' => 'checkbox',
]) ?>
    <div class="form-group">
        <div class="col-lg-10 col-lg-offset-2">
            <a href="<?php echo Url::toRoute(['/admin/products']); ?>" class="btn btn-warning">Вернуться к списку</a>
            <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('.glyphicon-remove').click(function() {
            var $this = $(this);
            var prod_id = <?php echo isset($model->id) ? $model->id : 0; ?>;
            var image_id = $this.data('image-id') ? $(this).data('image-id') : 0;
            var preview_id = $this.data('preview-id') ? $(this).data('preview-id') : 0;


            $.ajax({
                url: '<?php echo Url::to('delete-images'); ?>',
                type: 'post',
                dataType: 'json',
                data: {
                    prod_id: prod_id,
                    preview_id: preview_id,
                    _csrf: yii.getCsrfToken(),
                    image_id: image_id
                },
                success: function (response) {
                    if (response.status == true) {
                        if (preview_id) {
                            $this.parent('.preview.slides').find('input[type=hidden]').val('');
                            $this.parent('.preview.slides').find('img').attr('src', '<?php echo '/' . Yii::$app->params['pathToImage']; ?>')
                            $this.parent('.preview.slides').remove();
                        }
                        if (image_id) {
                            $this.parent('.slides').prev('label').remove();
                            $this.parent('.slides').remove();
                        }
                    }
                },
                error: function () {
                }
            });
        });
    });
</script>
