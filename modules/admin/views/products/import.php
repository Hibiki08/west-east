<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

?>
    <h1><?php echo $this->title; ?></h1>
<?php foreach(Yii::$app->session->getAllFlashes() as $key => $message) { ?>
    <div class="flash-<?php echo $key; ?>"><?php echo $message; ?></div>
<?php } ?>

<?php $form = ActiveForm::begin([
    'options' => ['enctype'=>'multipart/form-data', 'class' => 'form-horizontal',],
    'fieldConfig' => [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10">{input}</div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
<?php echo $form->field($edit, 'file', ['options' => [
    'id' => 'xml-file',
    'class' => 'form-group'
]])->fileInput(); ?>
    <div class="form-group">
        <div class="col-lg-10 col-lg-offset-2">
            <?php echo Html::submitButton('Импортировать', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>