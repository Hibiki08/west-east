<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\modules\admin\assets\AdminAsset;
use yii\widgets\Breadcrumbs;

AdminAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="fuelux">
<head>
    <meta charset="UTF-8">
    <?php echo Html::csrfMetaTags() ?>
    <title><?php echo $this->title; ?></title>
    <?php $this->head() ?>
    <link rel="icon" type="image/png" href="/images/16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/images/24.png" sizes="24x24">
    <link rel="icon" type="image/png" href="/images/32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/images/48.png" sizes="48x48">
    <link rel="icon" type="image/png" href="/images/64.png" sizes="64x64">
    <link rel="icon" href="/images/favicon.ico">
</head>
<body>
<?php $this->beginBody() ?>
<?php
NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);
if (Yii::$app->user->can('adminPermission')) {
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'encodeLabels' => false,
        'items' => [
            ['label' => 'Уволенные сторудники',
                'url' => ['#'],
                'items' => [
                    ['label' => 'Список', 'url' => ['/admin/fired-staff']],
                ],
            ],
            ['label' => 'Должники',
                'url' => ['#'],
                'items' => [
                    ['label' => 'Список', 'url' => ['/admin/debtors']],
                ],
            ],
            ['label' => 'Контакты',
                'url' => ['#'],
                'items' => [
                    ['label' => 'Филиалы', 'url' => ['/admin/contacts/branches']],
                    ['label' => 'Склады', 'url' => ['/admin/contacts/warehouses']],
                    ['label' => 'Дистрибьютеры', 'url' => ['/admin/contacts/distributors']],
                    ['label' => 'Города', 'url' => ['/admin/contacts/cities']],
                ],
            ],
            ['label' => 'Слайдеры',
                'url' => ['#'],
                'items' => [
                    ['label' => 'Главный слайдер', 'url' => ['/admin/slides']],
                ],
            ],
            ['label' => 'Блог',
                'url' => ['#'],
                'items' => [
                    ['label' => 'Новости', 'url' => ['/admin/news']],
                ],
            ],
            ['label' => 'Продукты',
                'url' => ['#'],
                'items' => [
                    ['label' => 'Продукты', 'url' => ['/admin/products']],
                    ['label' => 'Категории', 'url' => ['/admin/categories']],
                ],
            ],
            ['label' => 'Рецепты',
                'url' => ['#'],
                'items' => [
                    ['label' => 'Список', 'url' => ['/admin/recipes']],
                ],
            ],
            ['label' => 'Импорт',
                'url' => ['#'],
                'items' => [
                    ['label' => 'Импорт продуктов', 'url' => ['/admin/products/import']],
                    ['label' => 'Импорт категорий', 'url' => ['/admin/categories/import']],
                ],
            ],
            '<li id="exit">'
            . Html::beginForm(['/logout'], 'post')
            . Html::submitButton(
                'Выйти',
                ['class' => 'btn btn-secondary col-lg-12']
            )
            . Html::endForm()
            . '</li>'
        ],
    ]);
} 
NavBar::end();
?>
<div class="admin-container container">
    <div class="width">
        <?php echo Breadcrumbs::widget([
            'itemTemplate' => "<li><i>{link}</i></li>\n", // template for all links
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
        ]);
        ?>
        <div>
            <?php echo $content ?>
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
