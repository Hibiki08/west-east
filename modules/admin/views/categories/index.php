<?php
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use leandrogehlen\treegrid\TreeGrid;
?>

    <h1><?php echo Html::encode($this->title) ?></h1>
    <p><?php echo Html::a('Создать элемент', ['add'], ['class' => 'btn btn-success']); ?></p>

<?php echo TreeGrid::widget([
    'dataProvider' => $categories,
//    'filterModel' => $searchModel,
    'keyColumnName' => 'id',
//    'showOnEmpty' => false,
    'parentColumnName' => 'parent_id',
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
    'pluginOptions' => [
        'initialState' => 'collapsed',
    ],
    'columns' => [
//        'id',
        'title:text:Название',
        [
            'label' => 'SEO URL',
            'attribute' => 'seo_url',
            'value' => function ($model, $key, $index, $column) {
                return $model['seo_url'];
            },
            'format' => 'raw',
        ],
        ['class' => 'yii\grid\ActionColumn',
            'template' => '{add} {update} {delete}',
            'buttons' => [
                'add' => function ($url, $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-plus"></span>', $url);
                },
            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'add') {
                    $url = Url::to(['/admin/categories/add', 'id' => $model['id']]);
                    return $url;
                }

                if ($action === 'update') {
                    $url = Url::to(['/admin/categories/update', 'id' => $model['id']]);
                    return $url;
                }
                if ($action === 'delete') {
                    $url = Url::to(['/admin/categories/delete', 'id' => $model['id']]);
                    return $url;
                }
            }
        ]
    ],
]); ?>