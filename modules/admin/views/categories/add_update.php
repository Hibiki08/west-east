<?php
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use wbraganca\tagsinput\TagsinputWidget;

?>
    <h1><?php echo $this->title; ?></h1>

<?php $form = ActiveForm::begin([
    'options' => ['enctype'=>'multipart/form-data', 'class' => 'form-horizontal',],
    'fieldConfig' => [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10">{input}</div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
<?php echo $form->field($edit, 'title')->input('text', ['value' => $model->title]); ?>
<?php echo $form->field($edit, 'parent_id')->dropDownList($parents, ['options' => [$action == 'add' ? $id : $model['parent_id'] => ['selected' => true]]]); ?>
<?php echo $form->field($edit, 'description')->widget(CKEditor::className(), [
    'editorOptions' => ElFinder::ckeditorOptions(['elfinder'], [
        'preset' => 'full',
        'inline' => false,
    ]),
    'options' => [
        'value' => $model['description']
    ]
]); ?>
<?php echo $form->field($edit, 'seo_url')->input('text', ['value' => $model->seo_url]); ?>
<?php echo $form->field($edit, 'meta_title')->input('text', ['value' => $model['meta_title']]); ?>
<?php echo $form->field($edit, 'meta_description')->input('text', ['value' => $model['meta_description']]); ?>
<?php echo $form->field($edit, 'keywords')->input('text', ['value' => $model['keywords']]); ?>
<?php echo $form->field($edit, 'tags')->widget(TagsinputWidget::class, [
    'clientOptions' => [
        'trimValue' => true,
        'allowDuplicates' => false,
    ]
])->textInput([
    'value' => implode(',', $tags),
    'data-id' => $model->id
]); ?>
<?php echo $form->field($edit, 'status')->input('checkbox', [
    'checked' => $model['status'] == 1 ? 'checked' : false,
    'class' => 'checkbox',
]) ?>
    <div class="form-group">
        <div class="col-lg-10 col-lg-offset-2">
            <a href="<?php echo Url::toRoute(['/admin/categories']); ?>" class="btn btn-warning">Вернуться к списку</a>
            <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>