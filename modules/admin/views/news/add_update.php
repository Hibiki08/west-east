<?php

use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\forms\NewsForm;

?>
    <h1><?php echo $this->title; ?></h1>

<?php $form = ActiveForm::begin([
    'options' => ['enctype'=>'multipart/form-data', 'class' => 'form-horizontal',],
    'fieldConfig' => [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10">{input}</div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
<?php echo $form->field($edit, 'title')->input('text', ['value' => $model->title]); ?>
<?php echo $form->field($edit, 'text')->widget(CKEditor::className(), [
    'editorOptions' => ElFinder::ckeditorOptions(['elfinder'], [
        'preset' => 'full',
        'inline' => false,
    ]),
    'options' => [
        'value' => $model['text']
    ]
]); ?>
<?php echo $form->field($edit, 'meta_title')->input('text', ['value' => $model['meta_title']]); ?>
<?php echo $form->field($edit, 'meta_description')->input('text', ['value' => $model['meta_description']]); ?>
<?php echo $form->field($edit, 'keywords')->input('text', ['value' => $model['keywords']]); ?>
<?php echo $form->field($edit, 'preview', ['options' => [
    'id' => 'preview-file',
    'class' => 'form-group field-editnewsform-preview'
]])->fileInput()->label('Загрузить превью');
if (isset($model->preview)) { ?>
        <label class="col-lg-2 control-label"></label>
        <div class="slides">
            <figure>
                <img class="img-thumbnail" src="<?php echo Yii::$app->params['pathToImage'] . $imageDir . '/mini_' . $model->preview; ?>">
            </figure>
            <span class="glyphicon glyphicon-remove" data-new-id="<?php echo $model->id; ?>"></span>
        </div>
<?php } ?>
<?php echo $form->field($edit, 'active')->input('checkbox', [
    'checked' => $model['active'] == 1 ? 'checked' : false,
    'class' => 'checkbox',
]) ?>
    <div class="form-group">
        <div class="col-lg-10 col-lg-offset-2">
            <a href="<?php echo Url::toRoute(['/admin/news']); ?>" class="btn btn-warning">Вернуться к списку</a>
            <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>