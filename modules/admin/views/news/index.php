<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\jui\DatePicker;
use app\models\forms\NewsForm;

?>

    <h1><?php echo Html::encode($this->title); ?></h1>
    <p><?php echo Html::a('Добавить новость', ['update'], ['class' => 'btn btn-success']); ?></p>

<?php echo GridView::widget([
    'dataProvider' => $news,
    'filterModel' => $searchModel,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
    'summary' => '',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id:text:ID',
        [
            'attribute' => 'preview',
            'label' => 'Превью',
            'value' => function ($model, $key, $index, $column) use ($imageDir) {
                return !empty($model['preview']) ? '<img src="/images' . $imageDir . '/mini_' . $model['preview'] : '';
            },
            'format' => 'html',
        ],
        [
            'attribute' => 'title',
            'label' => 'Название',
            'value' => function ($model, $key, $index, $column) {
                return $model['title'];
            },
        ],
        [
            'label' => 'Дата создания',
            'attribute' => 'date',
            'filter' => '<div class="form-group">' . DatePicker::widget([
                    'language' => 'ru',
                    'model' => $searchModel,
                    'attribute' => 'date',
                    'dateFormat' => 'dd-MM-yyyy',
                    'options' => [
                        'class' => 'form-control'
                    ]
                ]). '</div>',
            'format' =>  ['date', 'dd-MM-Y HH:mm:ss']
        ],
        [
            'attribute' => 'active',
            'label' => 'Активность',
            'value' => function ($model, $key, $index, $column) {
                return $model['active'] ? 'Да' : 'Нет';
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
        ],
    ],
]); ?>