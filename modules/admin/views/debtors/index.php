<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

?>

    <h1><?php echo Html::encode($this->title); ?></h1>
    <p><?php echo Html::a('Добавить компанию', ['update'], ['class' => 'btn btn-success']); ?></p>

<?php echo GridView::widget([
    'dataProvider' => $debtors,
    'filterModel' => $searchModel,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
    'summary' => '',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id:text:ID',
        [
            'attribute' => 'company_name',
            'value' => function ($model, $key, $index, $column) {
                return $model['company_name'];
            },
        ],
        [
            'attribute' => 'ind_number',
            'value' => function ($model, $key, $index, $column) {
                return $model['ind_number'];
            },
        ],
        [
            'attribute' => 'reg_number',
            'value' => function ($model, $key, $index, $column) {
                return $model['reg_number'];
            },
        ],
        [
            'attribute' => 'address',
            'value' => function ($model, $key, $index, $column) {
                return $model['address'];
            },
        ],
        [
            'attribute' => 'member',
            'value' => function ($model, $key, $index, $column) {
                return $model['member'];
            },
        ],
        [
            'attribute' => 'director',
            'value' => function ($model, $key, $index, $column) {
                return $model['director'];
            },
        ],
        [
            'attribute' => 'court_decision',
            'value' => function ($model, $key, $index, $column) use ($docDir) {
                return !empty($file = $model['court_decision']) ? '<a href="' . Yii::$app->params['pathToDocs'] . $docDir . '/' . $file . '" target="_blank"><span class="glyphicon glyphicon-file"></span> Файл</a>' : '';
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'active',
            'label' => 'Активность',
            'value' => function ($model, $key, $index, $column) {
                return $model['active'] ? 'Да' : 'Нет';
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
        ],
    ],
]); ?>