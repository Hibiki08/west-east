<?php
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use wbraganca\tagsinput\TagsinputWidget;

/**
 * @var app\models\Debtor $model
 * @var app\models\forms\DebtorForm $edit
 */

?>
<h1><?php echo $this->title; ?></h1>

<?php $form = ActiveForm::begin([
    'options' => ['enctype'=>'multipart/form-data', 'class' => 'form-horizontal',],
    'fieldConfig' => [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10">{input}</div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
<?php echo $form->field($edit, 'company_name')->input('text', ['value' => $model['company_name']]); ?>
<?php $edit->address = !empty($model->address) ? $model->address : ''; echo $form->field($edit, 'address')->textarea(['rows' => '6']); ?>
<?php echo $form->field($edit, 'ind_number')->input('text', ['value' => $model['ind_number']]); ?>
<?php echo $form->field($edit, 'reg_number')->input('text', ['value' => $model['reg_number']]); ?>
<?php echo $form->field($edit, 'member')->input('text', ['value' => $model['member']]); ?>
<?php echo $form->field($edit, 'director')->input('text', ['value' => $model['director']]); ?>
<?php echo $form->field($edit, 'court_decision')->fileInput(); ?>
<?php if (!empty($model['court_decision'])) { ?>
    <div class="form-group field-debtorform-director required">
        <label class="col-lg-2 control-label"></label><p></p><label class="col-lg-2 control-label" for="debtorform-director"></label><div class="col-lg-10"><a href="<?php echo Yii::$app->params['pathToDocs'] . $docDir . '/' . $model['court_decision']; ?>" target="_blank"><span class="glyphicon glyphicon-file"> <?php echo $model['court_decision']; ?></span></a></div>
    </div>
<?php } ?>
<?php echo $form->field($edit, 'active')->input('checkbox', [
    'checked' => $model['active'] == 1 ? 'checked' : false,
    'class' => 'checkbox',
]) ?>
<div class="form-group">
    <div class="col-lg-10 col-lg-offset-2">
        <a href="<?php echo Url::toRoute(['/admin/debtors']); ?>" class="btn btn-warning">Вернуться к списку</a>
        <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>