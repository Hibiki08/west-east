<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\jui\DatePicker;
use app\models\forms\NewsForm;

?>

    <h1><?php echo Html::encode($slider['description']); ?></h1>
    <p><?php echo Html::a('Добавить слайд', ['update', 'type' => $slider['id']], ['class' => 'btn btn-success']); ?></p>

<?php echo GridView::widget([
    'dataProvider' => $slides,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
    'summary' => '',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id:text:ID',
        [
            'attribute' => 'img',
            'label' => 'Картинка',
            'value' => function ($model, $key, $index, $column) use ($imageDir) {
                return !empty($model['img']) ? '<img src="/images' . $imageDir . '/' . $model['type_id'] . '/mini_' . $model['img'] : '';
            },
            'format' => 'html',
        ],
        [
            'attribute' => 'link',
            'label' => 'Ссылка',
            'value' => function ($model, $key, $index, $column) {
                return $model['link'];
            },
        ],
        [
            'attribute' => 'sort',
            'label' => 'Сортировка',
            'value' => function ($model, $key, $index, $column) {
                return '<input type="text" class="form-control show_position" name="show_position" placeholder="очерёдность" value="' . $model['sort'] . '" data-id="' . $model['id'] . '">';
            },
            'format' => 'raw',
        ],
        [
            'attribute' => 'active',
            'label' => 'Активность',
            'value' => function ($model, $key, $index, $column) {
                return $model['active'] ? 'Да' : 'Нет';
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['/admin/slides/update', 'id' => $model['id'], 'type' => $model['type_id']]));
                },
                'delete' => function ($url, $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to(['/admin/slides/delete', 'id' => $model['id']]), [
                        'title' => 'Delete',
                        'aria-label' => 'Delete',
                        'data-confirm' => 'Are you sure you want to delete this item?'
                    ]);
                },
            ]
        ],
    ],
]); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $('input[name="show_position"]').change(function() {
            var $this = $(this);
            var value = $(this).val();

            $.ajax({
                url: '<?php echo Url::to('/admin/slides/sort'); ?>',
                type: 'post',
                dataType: 'json',
                data: {
                    id: $this.data().id,
                    value: value,
                    _csrf: yii.getCsrfToken()
                }
            });
        });
    });
</script>
