<?php
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
//use app\models\Slides;
/**
 * @var app\models\Slide $model
 * @var app\models\forms\SlideForm $edit
 */
?>
<h1><?php echo $this->title; ?></h1>

<?php $form = ActiveForm::begin([
    'options' => ['enctype'=>'multipart/form-data', 'class' => 'form-horizontal',],
    'fieldConfig' => [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10">{input}</div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
<?php echo $form->field($edit, 'text')->widget(CKEditor::className(),[
    'editorOptions' => [
        'preset' => 'full',
        'inline' => false,
    ],
    'options' => [
        'value' => $model->text
    ]
]); ?>
<?php echo $form->field($edit, 'link')->input('text', ['value' => $model->link]); ?>
<?php echo $form->field($edit, 'img')->fileInput()->label('Загрузить слайд'); ?>
<?php if (!empty($model->img)) { ?>
    <div class="form-group">
        <label class="col-lg-2"></label>
        <figure class="col-lg-10">
            <img class="img-thumbnail" src="<?php echo Yii::$app->params['pathToImage'] . $imageDir . '/' . $model->type_id . '/mini_' . $model->img; ?>">
        </figure>
    </div>
<?php } ?>
<?php echo $form->field($edit, 'sort')->input('text', ['value' => $model->sort]); ?>
<?php echo $form->field($edit, 'active')->input('checkbox', [
    'checked' => $model->active == 1 ? 'checked' : false,
    'class' => 'checkbox',
])->label('Активность'); ?>
<div class="form-group">
    <div class="col-lg-10 col-lg-offset-2">
        <a href="<?php echo Url::toRoute(['/admin/slides']); ?>" class="btn btn-warning">Вернуться к списку</a>
        <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

