<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

?>

    <h1><?php echo Html::encode($this->title); ?></h1>
    <p><?php echo Html::a('Добавить сотрудника', ['update'], ['class' => 'btn btn-success']); ?></p>

<?php echo GridView::widget([
    'dataProvider' => $staff,
    'filterModel' => $searchModel,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
    'summary' => '',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id:text:ID',
        [
            'attribute' => 'name',
            'value' => function ($model, $key, $index, $column) {
                return $model['name'];
            },
        ],
        [
            'attribute' => 'position',
            'value' => function ($model, $key, $index, $column) {
                return $model['position'];
            },
        ],
        [
            'attribute' => 'subdivision',
            'value' => function ($model, $key, $index, $column) {
                return $model['subdivision'];
            },
        ],
        [
            'attribute' => 'dis_reason',
            'value' => function ($model, $key, $index, $column) {
                return $model['dis_reason'];
            },
        ],
        [
            'attribute' => 'details',
            'value' => function ($model, $key, $index, $column) {
                return $model['details'];
            },
        ],
        [
            'attribute' => 'active',
            'label' => 'Активность',
            'value' => function ($model, $key, $index, $column) {
                return $model['active'] ? 'Да' : 'Нет';
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
        ],
    ],
]); ?>