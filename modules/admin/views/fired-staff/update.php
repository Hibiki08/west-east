<?php
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use wbraganca\tagsinput\TagsinputWidget;

/**
 * @var app\models\FiredStaff $model
 * @var app\models\forms\FiredStaffForm $edit
 */

?>
    <h1><?php echo $this->title; ?></h1>

<?php $form = ActiveForm::begin([
    'options' => ['enctype'=>'multipart/form-data', 'class' => 'form-horizontal',],
    'fieldConfig' => [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10">{input}</div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
<?php echo $form->field($edit, 'name')->input('text', ['value' => $model['name']]); ?>
<?php echo $form->field($edit, 'position')->input('text', ['value' => $model['position']]); ?>
<?php echo $form->field($edit, 'subdivision')->input('text', ['value' => $model['subdivision']]); ?>
<?php $edit->dis_reason = !empty($model->dis_reason) ? $model->dis_reason : ''; echo $form->field($edit, 'dis_reason')->textarea(['rows' => '6']); ?>
<?php $edit->details = !empty($model->details) ? $model->details : ''; echo $form->field($edit, 'details')->textarea(['rows' => '6']); ?>
<?php echo $form->field($edit, 'active')->input('checkbox', [
    'checked' => $model['active'] == 1 ? 'checked' : false,
    'class' => 'checkbox',
]) ?>
    <div class="form-group">
        <div class="col-lg-10 col-lg-offset-2">
            <a href="<?php echo Url::toRoute(['/admin/fired-staff']); ?>" class="btn btn-warning">Вернуться к списку</a>
            <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>