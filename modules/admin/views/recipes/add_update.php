<?php
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use wbraganca\tagsinput\TagsinputWidget;

/**
 * @var app\models\Recipe $model
 * @var app\models\forms\RecipesForm $edit
 */

?>
    <h1><?php echo $this->title; ?></h1>

<?php $form = ActiveForm::begin([
    'options' => ['enctype'=>'multipart/form-data', 'class' => 'form-horizontal',],
    'fieldConfig' => [
        'template' => '<label class="col-lg-2 control-label"></label>{error}{label}<div class="col-lg-10">{input}</div>',
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
<?php echo $form->field($edit, 'title')->input('text', ['value' => $model->title]); ?>
<?php echo $form->field($edit, 'text')->widget(CKEditor::className(), [
    'editorOptions' => ElFinder::ckeditorOptions(['elfinder'], [
        'preset' => 'full',
        'inline' => false,
    ]),
    'options' => [
        'value' => $model['text']
    ]
]); ?>
<?php echo $form->field($edit, 'meta_title')->input('text', ['value' => $model['meta_title']]); ?>
<?php echo $form->field($edit, 'meta_description')->input('text', ['value' => $model['meta_description']]); ?>
<?php echo $form->field($edit, 'keywords')->input('text', ['value' => $model['keywords']]); ?>
<?php echo $form->field($edit, 'seo_url')->input('text', ['value' => $model['seo_url']]); ?>
<?php echo $form->field($edit, 'preview', ['options' => [
    'id' => 'preview-file',
    'class' => 'form-group field-editnewsform-preview'
]])->fileInput()->label('Загрузить превью');
if (isset($model->preview)) { ?>
        <label class="col-lg-2 control-label"></label>
        <div class="slides preview">
            <figure>
                <img class="img-thumbnail" src="<?php echo Yii::$app->params['pathToImage'] . $imageDir . '/mini_' . $model->preview; ?>">
            </figure>
            <span class="glyphicon glyphicon-remove" data-preview-id="<?php echo $model->id; ?>"></span>
        </div>
<?php } ?>
<?php echo $form->field($edit, 'tags')->widget(TagsinputWidget::class, [
    'clientOptions' => [
        'trimValue' => true,
        'allowDuplicates' => false,
    ]
])->textInput([
    'value' => implode(',', $tags),
    'data-id' => $model->id
]); ?>
<?php echo $form->field($edit, 'active')->input('checkbox', [
    'checked' => $model['active'] == 1 ? 'checked' : false,
    'class' => 'checkbox',
]) ?>
    <div class="form-group">
        <div class="col-lg-10 col-lg-offset-2">
            <a href="<?php echo Url::toRoute(['/admin/recipes']); ?>" class="btn btn-warning">Вернуться к списку</a>
            <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('.glyphicon-remove').click(function() {
            var $this = $(this);
            var recipe_id = <?php echo isset($model->id) ? $model->id : 0; ?>;
            var preview_id = $this.data('preview-id') ? $(this).data('preview-id') : 0;


            $.ajax({
                url: '<?php echo Url::to('delete-preview'); ?>',
                type: 'post',
                dataType: 'json',
                data: {
                    recipe_id: recipe_id,
                    preview_id: preview_id,
                    _csrf: yii.getCsrfToken()
                },
                success: function (response) {
                    if (response.status == true) {
                        if (preview_id) {
                            $this.parent('.preview.slides').find('input[type=hidden]').val('');
                            $this.parent('.preview.slides').find('img').attr('src', '<?php echo '/' . Yii::$app->params['pathToImage']; ?>')
                            $this.parent('.preview.slides').remove();
                        }
                    }
                },
                error: function () {
                }
            });
        });
    });
</script>
