<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

?>

    <h1><?php echo Html::encode($this->title); ?></h1>
    <p><?php echo Html::a('Добавить рецепт', ['update'], ['class' => 'btn btn-success']); ?></p>

<?php echo GridView::widget([
    'dataProvider' => $recipes,
    'filterModel' => $searchModel,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
    'summary' => '',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id:text:ID',
        [
            'attribute' => 'preview',
            'label' => 'Превью',
            'value' => function ($model, $key, $index, $column) use ($imageDir) {
                return !empty($model['preview']) ? '<img src="/images' . $imageDir . '/mini_' . $model['preview'] : '';
            },
            'format' => 'html',
        ],
        [
            'attribute' => 'title',
//                'attribute' => 'productTitle',
            'label' => 'Название',
            'value' => function ($model, $key, $index, $column) {
                return $model['title'];
            },
        ],
        [
            'attribute' => 'seo_url',
            'label' => 'SEO URL',
            'value' => function ($model, $key, $index, $column) {
                return $model['seo_url'];
            },
        ],
        [
            'attribute' => 'active',
            'label' => 'Активность',
            'value' => function ($model, $key, $index, $column) {
                return $model['active'] ? 'Да' : 'Нет';
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
        ],
    ],
]); ?>